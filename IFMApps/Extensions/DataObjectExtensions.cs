﻿using IFMApps.Dtos;
using Newtonsoft.Json;
using System.Data;
using System.Reflection;
using System.Text.Json.Nodes;
using System.Xml;

namespace IFMApps.Extensions
{
    public static class DataObjectExtensions
    {
        public static T ToSubmitResponseDto<T>(this string jsonResponse)
        {
            var ResponseDto = JsonConvert.DeserializeObject<SubmitResponseDto<T>>(jsonResponse);
            return ResponseDto.DT1.FirstOrDefault();
        }
        public static T ToModelDto<T>(this string jsonResponse)
        {
            var ModelDto = JsonConvert.DeserializeObject<T>(jsonResponse);
            return ModelDto;
        }

        public static JsonArray ConvertToJsonArray<T>(this T model)
        {
            PropertyInfo[] propertyInfos = model.GetType().GetProperties();
            var jsonArray = new JsonArray();

            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                object value = propertyInfo.GetValue(model);
                jsonArray.Add(value);
            }

            return jsonArray;
        }

        public static DataSet ToDataSetDto(this string jsonResponse)
        {
            /// Note:Json convertor needs a json with one node as root
            jsonResponse = $"{{ \"rootNode\": {{{jsonResponse.Trim().TrimStart('{').TrimEnd('}')}}} }}";
            //// Now it is secure that we have always a Json with one node as root 
            var xd = JsonConvert.DeserializeXmlNode(jsonResponse);

            //// DataSet is able to read from XML and return a proper DataSet
            var result = new DataSet();
            result.ReadXml(new XmlNodeReader(xd), XmlReadMode.Auto);
            return result;
        }

        public static DateTime ToSingaporeTimezone(this DateTime dateTimeUtcNow)
        {
            DateTime DateSingapore = dateTimeUtcNow.AddHours(8);

            return DateSingapore;
        }
    }
}
