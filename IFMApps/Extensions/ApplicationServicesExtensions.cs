﻿using IFMApps.Data;
using IFMApps.Entities;
using IFMApps.Helpers;
using IFMApps.Interface;
using IFMApps.Services;
using Microsoft.EntityFrameworkCore;

namespace IFMApps.Extensions
{
    public static class ApplicationServicesExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<LogAPIActivity>();
            services.AddScoped<IAuthServices, AuthServices>();
            services.AddDbContext<IfmContext>(x => x.UseMySql(config.GetConnectionString("IFMConnection"), new MySqlServerVersion(new Version(5, 6, 50))), ServiceLifetime.Transient);

            services.AddAutoMapper(typeof(AutoMapperProfile).Assembly);
            return services;
        }
    }
}
