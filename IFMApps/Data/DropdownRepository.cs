﻿using IFMApps.Entities;
using IFMApps.Helpers;
using IFMApps.Interface;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace IFMApps.Data
{
    public class DropdownRepository : IDropdownRepository
    {
        private readonly IfmContext _context;
        public DropdownRepository(IfmContext context)
        {
            _context = context;
        }

        public List<SelectListItem> GetDrpFieldValue(string fieldType, string SelectedValue)
        {
            var fieldValues = _context.FieldValues.Where(o => o.Field == fieldType).ToList();
            var listItem = fieldValues.Select(i => new SelectListItem { Value = i.Value.ToString(), Text = i.Descr == null ? i.Value : i.Descr }).ToList();
            return listItem;
        }

        public List<SelectListItem> GetDrpGuards(string SelectedValue)
        {
            var fieldValues = _context.Users.Where(o => o.Role == IFMAppRole.User).AsQueryable();
            var listItem = fieldValues.Select(i => new SelectListItem { Value = i.Code.ToString(), Text = i.Name }).ToList();
            return listItem;
        }

        public async Task<SelectList> GetDrpItemOrganizations(string SelectedValue)
        {
            return new SelectList(await _context.Orgs.ToListAsync(), "Idorg", "Name", SelectedValue);
        }

        public List<SelectListItem> GetDrpItemPoint(string SelectedValue)
        {
            var fieldValues = _context.Points.AsQueryable();
            var listItem = fieldValues.Select(i => new SelectListItem { Value = i.Idpoint.ToString(), Text = i.Name }).ToList();
            return listItem;
        }

        public async Task<SelectList> GetDrpItemSites(string SelectedValue)
        {
            return new SelectList(await _context.Sites.ToListAsync(), "Site1", "Name", SelectedValue);
        }
    }
}
