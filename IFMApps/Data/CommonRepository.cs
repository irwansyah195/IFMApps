﻿using IFMApps.Interface;
using Newtonsoft.Json;
using System.Text;
using System.Text.Json.Nodes;

namespace IFMApps.Data
{
    public class CommonRepository : ICommonRepository
    {
        private readonly IConfiguration _configuration;

        public CommonRepository(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public async Task<string> GuardWebRequest(string RequestUrl, JsonArray parameter)
        {
            HttpClient client = new HttpClient();
            var request = new HttpRequestMessage(HttpMethod.Post, RequestUrl);
            request.Content = new StringContent(parameter.ToString(), Encoding.UTF8, "application/json");
            request.Headers.Add("API_token", _configuration["SecretKey"].ToString());
            var response = await client.SendAsync(request);
            var responseContent = await response.Content.ReadAsStringAsync();

            return responseContent;
        }

        public async Task<string> GuardWebRequest<T>(string RequestUrl, T parameter)
        {
            HttpClient client = new HttpClient();
            string jsonParam = JsonConvert.SerializeObject(parameter);
            var request = new HttpRequestMessage(HttpMethod.Post, RequestUrl);
            request.Content = new StringContent(jsonParam, Encoding.UTF8, "application/json");
            request.Headers.Add("API_token", _configuration["SecretKey"].ToString());
            var response = await client.SendAsync(request);
            var responseContent = await response.Content.ReadAsStringAsync();

            return responseContent;
        }

        public string GetAppSettingJson(string Section)
        {
            return _configuration[Section].ToString();
        }
    }
}
