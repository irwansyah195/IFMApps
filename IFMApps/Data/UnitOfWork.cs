﻿using AutoMapper;
using IFMApps.Entities;
using IFMApps.Interface;
using IFMApps.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace IFMApps.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IfmContext _context;
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly IMapper _mapper;
        public UnitOfWork(IfmContext context, IConfiguration configuration, IWebHostEnvironment webHostEnvironment, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
        }
        public ICommonRepository CommonRepository => new CommonRepository(_configuration);
        public IDropdownRepository DropdownRepository => new DropdownRepository(_context); 
        public IUserRepository UserRepository => new UserRepository(_context);
        public IReportIncidentServices ReportIncidentServices => new ReportIncidentServices(_configuration,_webHostEnvironment);

        public IReportRepository ReportRepository => new ReportRepository(_configuration, _webHostEnvironment);
    }
}
