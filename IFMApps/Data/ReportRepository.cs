﻿using IFMApps.Extensions;
using IFMApps.Helpers;
using IFMApps.Interface;
using IFMApps.Services;
using Microsoft.AspNetCore.Hosting;
using Serilog;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Text;
using System.Text.Json.Nodes;

namespace IFMApps.Data
{
    public class ReportRepository : IReportRepository
    {
        private readonly IConfiguration _configuration;
        private readonly IHostEnvironment _environment;
        public ReportRepository(IConfiguration configuration, IHostEnvironment environment)
        {
            _configuration = configuration;
            _environment = environment;
        }
        public void SaveHttpResponseAsFile(string RequestUrl, string FilePath, dynamic Parameter)
        {

            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(RequestUrl);
            httpRequest.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
            httpRequest.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
            httpRequest.ContentType = "application/x-www-form-urlencoded";
            httpRequest.Method = "POST";
            HttpWebResponse response = null;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            try
            {
                using (var streamWriter = httpRequest.GetRequestStream())
                {
                    Dictionary<string, dynamic> postParams = new Dictionary<string, dynamic>();
                    object objectParam = Parameter;
                    string[] propertyNames = objectParam.GetType().GetProperties().Select(p => p.Name).ToArray();
                    foreach (var prop in propertyNames)
                    {
                        object propValue = objectParam.GetType().GetProperty(prop).GetValue(objectParam, null);

                        postParams.Add(prop, propValue);
                    }
                    byte[] jsonAsBytes = Encoding.UTF8.GetBytes(string.Join("&", postParams.Select(pp => pp.Key + "=" + pp.Value)));
                    streamWriter.Write(jsonAsBytes, 0, jsonAsBytes.Length);

                }

                response = (HttpWebResponse)httpRequest.GetResponse();
            }
            catch (System.Net.WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                    response = (HttpWebResponse)ex.Response;
            }

            using (Stream responseStream = response.GetResponseStream())
            {
                Stream FinalStream = responseStream;

                using (var fileStream = System.IO.File.Create(FilePath))
                {
                    FinalStream.CopyTo(fileStream);
                }

                response.Close();
                FinalStream.Close();
            }
        }


        public async Task<string> GenerateSiteReport()
        {
            CommonRepository commonRepository = new CommonRepository(_configuration);
            try
            {
                var ListSite = await commonRepository.GuardWebRequest(IFMAPIUrl.DDSite, new JsonArray());
                var ListSiteDto = ListSite.ToDataSetDto();
                var DatePeriode = DateTime.Now.ToSingaporeTimezone();
                foreach (DataTable table in ListSiteDto.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        var Result = SaveAndUploadSiteReport(Convert.ToInt32(dr["idsite"]), DatePeriode);
                        Log.Information("Save Site Report Period : " + DatePeriode);
                        Log.Information("Id Site : " + Result.IdSite);
                        Log.Information("S3 Url : " + Result.S3Url);
                        Log.Information("DateTime Generate : " + Result.Date);
                        var rptSite = await commonRepository.GuardWebRequest(IFMAPIUrl.AddReportSite, Result.ConvertToJsonArray());
                        Log.Information("Add To Database : " + rptSite.ToString());
                    }
                }

                return "Report generated";
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
                return ex.ToString();
            }
        }

        public SiteReportResponse SaveAndUploadSiteReport(int IdSite, DateTime DatePeriode)
        {
            DateTime BackDateMonth = DatePeriode.AddMonths(-1);
            string DaysInMonth = DateTime.DaysInMonth(BackDateMonth.Year, BackDateMonth.Month).ToString();
            string RangePeriode = "1 to " + DaysInMonth + " " + BackDateMonth.ToString("MMMM yyyy");

            try
            {
                AWSServices aWSServices = new AWSServices(_configuration);
                string RequestId = DateTime.UtcNow.ToSingaporeTimezone().ToString("ddMMyyyyHHmmssfff");
                string RequestUrl = _configuration["BaseUrl"].ToString() + "Reporting/SiteReport";
                string TempFolder = _environment.ContentRootPath + @"\Temp\" + RequestId + @"\";

                if (!Directory.Exists(TempFolder))
                {
                    Directory.CreateDirectory(TempFolder);
                }

                string FilePath = TempFolder + IdSite + "_SiteReport_" + RequestId + ".pdf";
                SaveHttpResponseAsFile(RequestUrl, FilePath, new { IdSite = IdSite, Date = DatePeriode.ToString("yyyy-MM-dd") });

                string response = string.Empty;

                using (FileStream filestream = System.IO.File.Open(FilePath, FileMode.Open))
                {
                    string s3DirectoryName = "T99";
                    string s3FileName = IdSite + "_SiteReport_" + RequestId + ".pdf";
                    //response = "http";
                    response = aWSServices.UploadFileToS3(filestream, s3DirectoryName, s3FileName);
                }

                System.IO.Directory.Delete(TempFolder, true);

                return new SiteReportResponse
                {
                    Date = RangePeriode,
                    S3Url = response,
                    IdSite = IdSite,
                    Type = "Attendance"
                };

            }
            catch (Exception ex)
            {
                return new SiteReportResponse
                {
                    Date = RangePeriode,
                    S3Url = ex.ToString(),
                    IdSite = IdSite,
                    Type = "Attendance"
                };
            }

        }

        public string SaveHttpResponseAsBase64(string RequestUrl, dynamic Parameter)
        {
            string Base64Response = string.Empty;
            HttpWebRequest httpRequest = (HttpWebRequest)WebRequest.Create(RequestUrl);
            httpRequest.UserAgent = "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)";
            httpRequest.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
            httpRequest.ContentType = "application/x-www-form-urlencoded";
            httpRequest.Method = "POST";
            HttpWebResponse response = null;
            ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
            try
            {
                using (var streamWriter = httpRequest.GetRequestStream())
                {
                    Dictionary<string, dynamic> postParams = new Dictionary<string, dynamic>();
                    object objectParam = Parameter;
                    string[] propertyNames = objectParam.GetType().GetProperties().Select(p => p.Name).ToArray();
                    foreach (var prop in propertyNames)
                    {
                        object propValue = objectParam.GetType().GetProperty(prop).GetValue(objectParam, null);

                        postParams.Add(prop, propValue);
                    }
                    byte[] jsonAsBytes = Encoding.UTF8.GetBytes(string.Join("&", postParams.Select(pp => pp.Key + "=" + pp.Value)));
                    streamWriter.Write(jsonAsBytes, 0, jsonAsBytes.Length);

                }

                response = (HttpWebResponse)httpRequest.GetResponse();
            }
            catch (System.Net.WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                    response = (HttpWebResponse)ex.Response;
            }

            using (Stream responseStream = response.GetResponseStream())
            {
                Stream FinalStream = responseStream;

                byte[] bytes;
                using (var memoryStream = new MemoryStream())
                {
                    responseStream.CopyTo(memoryStream);
                    bytes = memoryStream.ToArray();
                }

                Base64Response = Convert.ToBase64String(bytes);

                response.Close();
                FinalStream.Close();
            }

            return Base64Response;
        }


    }
}
