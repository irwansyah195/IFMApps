﻿using IFMApps.Entities;
using IFMApps.Extensions;
using IFMApps.Interface;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;

namespace IFMApps.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly IfmContext _context;
        public UserRepository(IfmContext context)
        {
            _context = context;
        }
        public async Task<string> GetClockInSiteToday(ClaimsPrincipal existingClaims)
        {
            string SiteToday = string.Empty;
            var claimsSite = existingClaims.FindFirst("Site")?.Value;
            if (claimsSite == null)
            {
                DateTime dateTime = DateTime.UtcNow.ToSingaporeTimezone();
                DateOnly dateOnly = new DateOnly(dateTime.Year, dateTime.Month, dateTime.Day);
                var claimsAttend = await _context.Attends.Where(o => o.Pers == existingClaims.FindFirst(ClaimTypes.NameIdentifier).Value && o.Date == dateOnly && o.Timein.HasValue).FirstOrDefaultAsync();
                if (claimsAttend != null)
                {
                    SiteToday = claimsAttend.Idsite.ToString();
                }
            }
            else
            {
                SiteToday = claimsSite;
            }

            return SiteToday;
        }
    }
}
