using Serilog.Events;
using Serilog;
using IFMApps.Extensions;
using Rotativa.AspNetCore;
using Quartz;
using IFMApps.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
builder.Services.AddMvc().AddRazorRuntimeCompilation();

// Logging
Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .MinimumLevel.Override("Microsoft.EntityFrameworkCore.Database.Command", LogEventLevel.Warning)
    .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
    .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
    .WriteTo.File("logs/ApplicationLog_.txt", rollingInterval: RollingInterval.Day)
    .CreateLogger();

builder.Logging.ClearProviders();
builder.Services.AddLogging(loggingBuilder =>
          loggingBuilder.AddSerilog(dispose: true));
// End Logging
builder.Services.AddApplicationServices(builder.Configuration);
builder.Services.AddIdentityServices(builder.Configuration);

builder.Services.AddQuartz(q =>
{
    q.UseMicrosoftDependencyInjectionJobFactory();

    var GenSiteReport = new JobKey("SiteReportSchedule");
    q.AddJob<QuartzScheduler>(opts => opts.WithIdentity(GenSiteReport));
    q.AddTrigger(opts => opts
        .ForJob(GenSiteReport)
        .WithIdentity("SiteReportSchedule-trigger")
        //.WithCronSchedule("0 0/2 * 1/1 * ? *")
        .WithCronSchedule("0 00 05 05 1/1 ? *")
        .StartNow());
});
builder.Services.AddQuartzHostedService(
    q => q.WaitForJobsToComplete = true);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();




app.UseAuthorization();

IWebHostEnvironment environment = builder.Environment;
RotativaConfiguration.Setup(environment.WebRootPath);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Account}/{action=Login}/{returnUrl?}");

app.MapControllerRoute(
        name: "excel",
        pattern: "excel",
        defaults: new { controller = "Home", action = "ViewExcel" });
app.Run();