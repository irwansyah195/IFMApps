﻿using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;
using OfficeOpenXml;
using IFMApps.Helpers;
using System.Security.Claims;
using System.Text.Json.Nodes;
using IFMApps.Interface;
using IFMApps.Extensions;
using System.Data;
using Serilog;
using System.Net;
using static System.Collections.Specialized.BitVector32;
using IFMApps.Services;

namespace IFMApps.APIController
{
    [AuthorizeRequest]
    public class ReportController : BaseAPIController
    {
        private readonly IUnitOfWork _uow;
        private readonly IConfiguration _configuration;
        private readonly IHostEnvironment _hostEnvironment;
        public ReportController(IUnitOfWork uow, IConfiguration configuration, IHostEnvironment hostEnvironment)
        {
            _uow = uow;
            _configuration = configuration;
            _hostEnvironment = hostEnvironment;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("ReportPayroll")]
        public async Task<JsonResult> ReportPayroll([FromQuery] string Section, string DateParam)
        {
            try
            {
                JsonArray jsonArray = new()
                {
                    DateParam
                };

                var rptPayroll = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.Payroll, jsonArray);
                var rptPayrollDataSet = rptPayroll.ToDataSetDto();
                var DataForExport = new DataTable();
                string SheetName = string.Empty;
                if (Section.ToLower() == "detail")
                {
                    DataForExport = rptPayrollDataSet.Tables[0];
                }
                else if (Section.ToLower() == "summary")
                {
                    DataForExport = rptPayrollDataSet.Tables[1];
                }
                else
                    return Json(new APIResponseResult((int)HttpStatusCode.InternalServerError, "", "error :  invalid section"));



                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage excel = new ExcelPackage())
                {
                    ////Start Highlight
                    ExcelWorksheet wsHighlight = excel.Workbook.Worksheets.Add(Section);

                    // Write the column headers
                    int colIndex = 1;
                    foreach (DataColumn dc in DataForExport.Columns)
                    {
                        wsHighlight.Cells[1, colIndex].Value = dc.ColumnName;
                        wsHighlight.Cells[1, colIndex].Style.Font.Bold = true;
                        wsHighlight.Cells[1, colIndex].Style.Font.Size = 11;
                        wsHighlight.Cells[1, colIndex].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        wsHighlight.Cells[1, colIndex].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
                        colIndex++;
                    }

                    // Write the data rows
                    int rowIndex = 2;
                    foreach (DataRow dr in DataForExport.Rows)
                    {
                        colIndex = 1;
                        foreach (DataColumn dc in DataForExport.Columns)
                        {
                            wsHighlight.Cells[rowIndex, colIndex].Value = dr[dc.ColumnName].ToString();
                            colIndex++;
                        }
                        rowIndex++;
                    }

                    wsHighlight.Cells[wsHighlight.Dimension.Address].AutoFitColumns();


                    return Json(new APIResponseResult((int)HttpStatusCode.OK, Convert.ToBase64String(excel.GetAsByteArray()), "OK"));
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return Json(new APIResponseResult((int)HttpStatusCode.InternalServerError, "", "error : " + ex.ToString()));
            }

        }

        [HttpGet("GenReportSite")]
        public async Task<JsonResult> ReportSite()
        {
            var Result = await _uow.ReportRepository.GenerateSiteReport();

            if (Result.Contains("Report generated"))
            {
                return Json(new APIResponseResult((int)HttpStatusCode.OK, "site report success generated", "OK"));
            }
            else
            {
                return Json(new APIResponseResult((int)HttpStatusCode.InternalServerError, Result, "error : generate report failed"));
            }
        }

        [HttpGet("QRPrintPage")]
        public JsonResult QRPrintPage([FromQuery] int Id, string section)
        {
            try
            {
                string Bas64File = string.Empty;
                string EndPointReport = string.Empty;
                if (section.ToLower() == "singlepoint")
                    EndPointReport = _configuration["BaseUrl"].ToString() + "Reporting/SinglePointQR";
                else if (section.ToLower() == "singlemonitor")
                    EndPointReport = _configuration["BaseUrl"].ToString() + "Reporting/SingleMonitorQR";
                else if (section.ToLower() == "allmonitor")
                    EndPointReport = _configuration["BaseUrl"].ToString() + "Reporting/AllMonitorQR";
                else if (section.ToLower() == "allpoint")
                    EndPointReport = _configuration["BaseUrl"].ToString() + "Reporting/AllPointQR";
                else
                    return Json(new APIResponseResult((int)HttpStatusCode.InternalServerError, "", "error :  invalid section"));

                Bas64File = _uow.ReportRepository.SaveHttpResponseAsBase64(EndPointReport, new { Id = Id });
                return Json(new APIResponseResult((int)HttpStatusCode.OK, Bas64File, "OK"));

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return Json(new APIResponseResult((int)HttpStatusCode.InternalServerError, "", "error : " + ex.ToString()));
            }
        }

        [HttpGet("ReportSite2")]
        public JsonResult ReportSite2([FromQuery] int IdSite, string Periode)
        {

            try
            {
                string Bas64File = string.Empty;
                string EndPointReport = _configuration["BaseUrl"].ToString() + "Reporting/SiteReport2";


                Bas64File = _uow.ReportRepository.SaveHttpResponseAsBase64(EndPointReport, new { IdSite = IdSite, Periode = Periode });
                return Json(new APIResponseResult((int)HttpStatusCode.OK, Bas64File, "OK"));

            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString());
                return Json(new APIResponseResult((int)HttpStatusCode.InternalServerError, "", "error : " + ex.ToString()));
            }
        }

        [HttpGet("ViewExcel")]
        public JsonResult ViewExcelReport([FromQuery] string S3Url)
        {
            try
            {
                var driveServices = new GDriveServices(_hostEnvironment);
                var FileName = Path.GetFileName(S3Url);

                var UrlDrive = driveServices.SaveFile(S3Url, FileName);
                return Json(new APIResponseResult((int)HttpStatusCode.OK, UrlDrive, "OK"));
            }
            catch(Exception ex)
            {
                Log.Error(ex.ToString());
                return Json(new APIResponseResult((int)HttpStatusCode.InternalServerError, "", "error : " + ex.ToString()));
            }
        }
    }
}
