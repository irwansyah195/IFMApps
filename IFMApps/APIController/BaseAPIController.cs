﻿using IFMApps.Helpers;
using Microsoft.AspNetCore.Mvc;

namespace IFMApps.APIController
{
    [ServiceFilter(typeof(LogAPIActivity))]
    [Route("api/[controller]")]
    [ApiController]
    public class BaseAPIController : Controller
    {
    }
}
