﻿namespace IFMApps.Helpers
{
    public class APIResponseResult
    {
        public APIResponseResult(int code, string result = null, string status = null)
        {
            Code = code;
            Result = result;
            Status = status;
        }

        public int Code { get; set; }
        public string Result { get; set; }
        public string Status { get; set; }
    }
}
