﻿namespace IFMApps.Helpers
{
    public static class IFMAPIUrl
    {
        public static string SignIn = "https://tklcloud.com/ifmx/api/sign_in";
        public static string ChangePassword = "https://tklcloud.com/ifmx/api/change_pwd";

        public static string ViewPers = "https://tklcloud.com/ifmx/api/view_pers";
        public static string PersAvail = "https://tklcloud.com/ifmx/api/pers_avail";
        public static string ViewPatrol = "https://tklcloud.com/ifmx/api/view_patrol";
        public static string ScanPatrol = "https://tklcloud.com/ifmx/api/scan_patrol";
        public static string ScanPatrol2 = "https://tklcloud.com/ifmx/api/scan_patrol2";
        public static string ScanPatrol3 = "https://tklcloud.com/ifmx/api/scan_patrol3";
        public static string ScanPatrolQR = "https://tklcloud.com/ifmx/api/scan_qr";
        public static string ScanPatrolNFC = "https://tklcloud.com/ifmx/api/scan_nfc";

        public static string ClockIn = "https://tklcloud.com/ifmx/api/clockin";
        public static string ClockOut = "https://tklcloud.com/ifmx/api/clockout";
        public static string TakeAttend = "https://tklcloud.com/ifmx/api/take_attend";
        public static string ViewSguide = "https://tklcloud.com/ifmx/api/view_sguide";
        public static string UpdateLeave = "https://tklcloud.com/ifmx/api/update_leave";
        public static string UpdateCiPhoto = "https://tklcloud.com/ifmx/api/update_ciphoto";
        public static string ReportFault = "https://tklcloud.com/ifmx/api/report_fault";
        public static string ReportFault2 = "https://tklcloud.com/ifmx/api/report_fault2";
        public static string ViewFault = "https://tklcloud.com/ifmx/api/view_fault";
        public static string DeleteFault = "https://tklcloud.com/ifmx/api/delete_fault";

        public static string Payroll = "https://tklcloud.com/ifmx/api/payroll";
        public static string ReportSite = "https://tklcloud.com/ifmx/api/site_rept";
        public static string ReportSite2 = "https://tklcloud.com/ifmx/api/site_rept2";
        public static string DDSite = "https://tklcloud.com/ifmx/api/dd_site";
        public static string NextPoint = "https://tklcloud.com/ifmx/api/next_point";
        public static string SkipPoint = "https://tklcloud.com/ifmx/api/skip_point";
        public static string AddReportSite = "https://tklcloud.com/ifmx/api/add_siterept";
        public static string MonitorQR2 = "https://tklcloud.com/ifmx/api/monitor_qr2";
        public static string MonitorQR = "https://tklcloud.com/ifmx/api/monitor_QR";
        public static string PointQR2 = "https://tklcloud.com/ifmx/api/point_qr2";
        public static string PointQR = "https://tklcloud.com/ifmx/api/point_QR";
        public static string QRTester = "https://tklcloud.com/ifmx/api/test_QR";


        public static string FindSite2 = "https://tklcloud.com/ifmx/api/find_site2";
        public static string FindPoint = "https://tklcloud.com/ifmx/api/find_point";
        public static string UpdatePoint2 = "https://tklcloud.com/ifmx/api/update_point2";

        public static string UpdatePoint3 = "https://tklcloud.com/ifmx/api/update_point3";
        public static string UpdateNFC = "https://tklcloud.com/ifmx/api/update_nfc";

    }
}
