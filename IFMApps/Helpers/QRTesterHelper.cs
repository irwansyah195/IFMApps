﻿namespace IFMApps.Helpers
{
    public class QRTesterParam
    {
        public string QR { get; set; }
        public string Lng { get; set; }
        public string Lat { get; set; }

    }
}
