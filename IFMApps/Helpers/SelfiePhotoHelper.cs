﻿namespace IFMApps.Helpers
{
    public class SelfiePhotoUpdateParam
    {
        public int Idciphoto { get; set; }
        public string Date { get; set; }
        public string Pers { get; set; }
        public string Photo { get; set; }
    }
}
