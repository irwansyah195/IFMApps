﻿namespace IFMApps.Helpers
{
    public static class IFMAppRole
    {
        public static string SuperAdmin = "Z";
        public static string Admin = "A";
        public static string Supervisor = "S";
        public static string User = "U";
        public static string Guard = "G";

    }
}
