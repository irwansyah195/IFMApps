﻿using IFMApps.Dtos;

namespace IFMApps.Helpers
{
    public class ClockInOutParam
    {
        public string QROrNFC { get; set; }
        public string Pers { get; set; }
        public string DateTime { get; set; }
        public string Lng { get; set; }
        public string Lat { get; set; }
    }

    public class ClockInOutResponse : BaseResponseDto
    {
        public int idattend1 { get; set; }
    }
}
