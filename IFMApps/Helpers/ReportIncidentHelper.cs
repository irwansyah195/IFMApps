﻿using IFMApps.Dtos;

namespace IFMApps.Helpers
{
    public class ReportIncidentParam
    {
        public string Pers { get; set; }
        public int Idincident { get; set; }
        public string Description { get; set; }
        public IFormFile File { get; set; }
    }

    public class ReportIncidentParamsWebRequest
    {
        public string Site { get; set; }
        public string Pers { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
    }

    public class ReportIncidentResponse : BaseResponseDto
    {
        public int? Idincident1 { get; set; }
    }
}
