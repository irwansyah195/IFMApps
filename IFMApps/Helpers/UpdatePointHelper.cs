﻿namespace IFMApps.Helpers
{
    public class UpdatePoint2Param
    {
        public string IdPoint { get; set; }
        public string Lng { get; set; }
        public string Lat { get; set; }
    }

    public class UpdatePoint3Param
    {
        public string IdPoint { get; set; }
        public string BeaconId { get; set; }
    }

    public class UpdateNFCParam
    {
        public string IdPoint { get; set; }
        public string NFC { get; set; }

    }
}
