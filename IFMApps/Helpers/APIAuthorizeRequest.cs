﻿using IFMApps.Interface;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace IFMApps.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeRequestAttribute : Attribute, IAuthorizationFilter
    {

        public void OnAuthorization(AuthorizationFilterContext context)
        {

            var _uow = context.HttpContext.RequestServices.GetRequiredService<IUnitOfWork>();

            string APIToken = string.Empty, RequestAPIToken = string.Empty;
            var GetTokenKey = _uow.CommonRepository.GetAppSettingJson("SecretKey");
            if (GetTokenKey != null)
                APIToken = GetTokenKey;

            if (context.HttpContext.Request.Headers.TryGetValue("API_token", out var tokenVal))
            {
                RequestAPIToken = tokenVal.First().Trim();
            }

            if (RequestAPIToken != APIToken || string.IsNullOrEmpty(APIToken))
            {

                var response = new APIResponseResult((int)HttpStatusCode.Unauthorized, "Invalid Token header", "Please provide a valid Token header");

                context.Result = new JsonResult(response)
                {
                    StatusCode = (int)HttpStatusCode.Unauthorized
                };
            }

        }
    }
}
