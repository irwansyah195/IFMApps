﻿using IFMApps.Dtos;
using System.ComponentModel.DataAnnotations;

namespace IFMApps.Helpers
{
    public class LeaveSubmissionParam
    {
        public int LeaveId { get; set; }
        public string Pers { get; set; }
        public string LeaveType { get; set; }
        [Required]
        public string LeaveFrom { get; set; }
        [Required]
        public string LeaveTo { get; set; }
    }
    public class LeaveSubmissionResponse : BaseResponseDto
    {
        public int idleave1 { get; set; }
    }
}
