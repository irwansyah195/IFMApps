﻿namespace IFMApps.Helpers
{
    public class SiteReportResponse
    {
        public int IdSite { get; set; }
        public string Type { get; set; }
        public string Date { get; set; }
        public string S3Url { get; set; }
    }
}
