﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace IFMApps.Helpers
{
    public class LogAPIActivity : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var resultContext = await next();

            //if (!resultContext.HttpContext.User.Identity.IsAuthenticated) return;


            string ControllerName = ((ControllerBase)context.Controller).ControllerContext.ActionDescriptor.ControllerName;
            string ActionName = ((ControllerBase)context.Controller).ControllerContext.ActionDescriptor.ActionName;
            string RouteParam = string.Join("|", context.RouteData.Values.Values.ToList());
            string LogInformation = string.Format("Controller : {0} , Action : {1} , Route : {2}", ControllerName, ActionName, RouteParam);
            Log.Information(LogInformation);
        }
    }
}
