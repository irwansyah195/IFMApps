﻿using IFMApps.Dtos;

namespace IFMApps.Helpers
{
    public class ScanPatrolParam
    {
        public string QROrNFC { get; set; }
        public string Pers { get; set; }

        public string DateTime { get; set; }

        public string Lng { get; set; }
        public string Lat { get; set; }
    }

    public class ScanPatrolNFCParam
    {
        public string NFC { get; set; }
        public string Pers { get; set; }

        public string DateTime { get; set; }
    }

    public class ScanPatrol3Param
    {
        public string IdPatrol { get; set; }
        public string QR { get; set; }

        public string Time { get; set; }

        public string Lng { get; set; }
        public string Lat { get; set; }
    }

    public class ScanPatrolResponse : BaseResponseDto
    {
        public int? idpoint1 { get; set; }
        public int? idpatrol1 { get; set; }
    }

    public class ScanPatrolResponse2 : BaseResponseDto
    {
        public int? idpatrol1 { get; set; }

    }

    public class ViewPatrolParam
    {
        public string Date { get; set; }
        public string Pers { get; set; }
    }
}
