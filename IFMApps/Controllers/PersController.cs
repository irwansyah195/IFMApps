﻿using IFMApps.Dtos;
using IFMApps.Entities;
using IFMApps.Extensions;
using IFMApps.Helpers;
using IFMApps.Interface;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Security.Claims;
using System.Text.Json.Nodes;

namespace IFMApps.Controllers
{
    [Authorize]
    public class PersController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly IAuthServices _authService;
        private readonly IConfiguration _configuration;

        public PersController(IUnitOfWork uow, IAuthServices authService, IConfiguration configuration)
        {
            _configuration = configuration;
            _uow = uow;
            _authService = authService;
        }
        public async Task<IActionResult> Index()
        {
            JsonArray jsonArray = new()
                {
                    User.FindFirst(ClaimTypes.NameIdentifier)?.Value
                };
            var ViewGuard = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ViewPers, jsonArray);
            var ViewGuardModel = ViewGuard.ToDataSetDto();

            if (TempData["Function"] != null)
            {
                ViewBag.Function = TempData["Function"].ToString();
            }

            return View(ViewGuardModel);
        }

        public async Task<IActionResult> PersAvail()
        {
            JsonArray jsonArray = new()
                {
                    User.FindFirst(ClaimTypes.NameIdentifier)?.Value
                };
            var ViewPers = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.PersAvail, jsonArray);
            var ViewPersModel = ViewPers.ToDataSetDto();

            if (TempData["Function"] != null)
            {
                ViewBag.Function = TempData["Function"].ToString();
            }

            return View(ViewPersModel);

        }

        public async Task<IActionResult> TakeAttend(int idAttend)
        {
            JsonArray jsonArray = new()
                {
                    idAttend,
                    User.FindFirst(ClaimTypes.NameIdentifier)?.Value
                };
            var TakeAttend = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.TakeAttend, jsonArray);
            var TakeAttendModel = TakeAttend.ToSubmitResponseDto<BaseResponseDto>();

            TempData["Function"] = string.Format("SwalAlert('{0}', '{1}', '{2}');", TakeAttendModel.Msg, TakeAttendModel.Msg2, "info");

            return RedirectToAction(nameof(PersAvail));
        }

        public IActionResult Leave()
        {
            ViewBag.LeaveType = _uow.DropdownRepository.GetDrpFieldValue("leave_type", "");

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Leave(LeaveSubmissionParam leaveSubmissionParams)
        {
            ViewBag.LeaveType = _uow.DropdownRepository.GetDrpFieldValue("leave_type", "");

            if (ModelState.IsValid)
            {
                leaveSubmissionParams.Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                var jsonArrayParam = leaveSubmissionParams.ConvertToJsonArray<LeaveSubmissionParam>();
                var LeaveResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.UpdateLeave, jsonArrayParam); ;
                var LeaveResponseModel = LeaveResponse.ToSubmitResponseDto<LeaveSubmissionResponse>();

                if (LeaveResponseModel.Msg == "OK")
                    ViewBag.Function = string.Format("SwalAlert('{0}', '{1}', '{2}');", "Submit Leave success", LeaveResponseModel.Msg2, "success");
                else
                    ViewBag.Function = string.Format("SwalAlert('{0}', '{1}', '{2}');", "Submit Leave fail", LeaveResponseModel.Msg2, "error");

            }


            return View();
        }

        [HttpPost]
        public async Task<JsonResult> ClockIn(ClockInOutParam clockInOutParams)
        {
            clockInOutParams.Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var jsonArrayParam = clockInOutParams.ConvertToJsonArray<ClockInOutParam>();
            var ClockInResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ClockIn, jsonArrayParam);
            var ClockInResponseModel = ClockInResponse.ToSubmitResponseDto<ClockInOutResponse>();

            var updatedClaimsIdentity = _authService.AddSiteToClaims(User, clockInOutParams.QROrNFC);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(updatedClaimsIdentity));

            return Json(ClockInResponseModel);
        }

        [HttpPost]
        public async Task<JsonResult> ClockOut(ClockInOutParam clockInOutParams)
        {
            clockInOutParams.Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;

            var jsonArrayParam = clockInOutParams.ConvertToJsonArray<ClockInOutParam>();
            var ClockInResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ClockOut, jsonArrayParam);
            var ClockInResponseModel = ClockInResponse.ToSubmitResponseDto<ClockInOutResponse>();
            return Json(ClockInResponseModel);
        }

        public async Task<IActionResult> Patrol()
        {
            var PatrolParams = new ViewPatrolParam()
            {
                Date = DateTime.UtcNow.ToSingaporeTimezone().ToString("yyyy-MM-dd"),
                Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
            };
            //var PatrolParams = new ViewPatrolParam()
            //{
            //    Date = "2023-05-22",
            //    Pers = "6004"
            //};
            var jsonArrayParam = PatrolParams.ConvertToJsonArray<ViewPatrolParam>();
            var PatrolResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.NextPoint, jsonArrayParam);
            var PatrolResponseModel = PatrolResponse.ToDataSetDto();

            if (TempData["Function"] != null)
            {
                ViewBag.Function = TempData["Function"].ToString();
            }

            return View(PatrolResponseModel);
        }

        [HttpPost]
        public async Task<JsonResult> ScanPatrol(ScanPatrolParam scanPatrolParams)
        {
            var SiteClockIn = await _uow.UserRepository.GetClockInSiteToday(User);
            if (string.IsNullOrEmpty(SiteClockIn))
            {
                BaseResponseDto baseResponseDto = new BaseResponseDto()
                {
                    Msg = "Scan Patrol Denied",
                    Msg2 = "Please do clockin first !"
                };

                return Json(baseResponseDto);
            }

            scanPatrolParams.Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var jsonArrayParam = scanPatrolParams.ConvertToJsonArray<ScanPatrolParam>();
            var scanPatrolResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ScanPatrol, jsonArrayParam);
            var scanPatrolResponseModel = scanPatrolResponse.ToModelDto<ScanPatrolResponseDto>();
            if (scanPatrolResponseModel.DT2 != null)
            {
                return Json(scanPatrolResponseModel.DT2.FirstOrDefault());
            }
            else
            {
                return Json(scanPatrolResponseModel.DT1.FirstOrDefault());
            }
        }


        [HttpPost]
        public async Task<JsonResult> ScanPatrol2(ScanPatrolParam scanPatrolParams)
        {
            var SiteClockIn = await _uow.UserRepository.GetClockInSiteToday(User);
            if (string.IsNullOrEmpty(SiteClockIn))
            {
                BaseResponseDto baseResponseDto = new BaseResponseDto()
                {
                    Msg = "Scan Patrol Denied",
                    Msg2 = "Please do clockin first !"
                };

                return Json(baseResponseDto);
            }

            scanPatrolParams.Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var jsonArrayParam = scanPatrolParams.ConvertToJsonArray<ScanPatrolParam>();
            var scanPatrolResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ScanPatrol2, jsonArrayParam);
            var scanPatrolResponseModel = scanPatrolResponse.ToModelDto<ScanPatrolResponseDto>();
            if (scanPatrolResponseModel.DT2 != null)
            {
                return Json(scanPatrolResponseModel.DT2.FirstOrDefault());
            }
            else
            {
                return Json(scanPatrolResponseModel.DT1.FirstOrDefault());
            }
        }

        [HttpPost]
        public async Task<JsonResult> ScanPatrol3(ScanPatrol3Param scanPatrolParams)
        {
            var SiteClockIn = await _uow.UserRepository.GetClockInSiteToday(User);
            if (string.IsNullOrEmpty(SiteClockIn))
            {
                BaseResponseDto baseResponseDto = new BaseResponseDto()
                {
                    Msg = "Scan Patrol Denied",
                    Msg2 = "Please do clockin first !"
                };

                return Json(baseResponseDto);
            }

            var jsonArrayParam = scanPatrolParams.ConvertToJsonArray<ScanPatrol3Param>();
            var scanPatrolResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ScanPatrol3, jsonArrayParam);
            var scanPatrolResponseModel = scanPatrolResponse.ToModelDto<ScanPatrolResponseDto>();
            if (scanPatrolResponseModel.DT2 != null)
            {
                return Json(scanPatrolResponseModel.DT2.FirstOrDefault());
            }
            else
            {
                return Json(scanPatrolResponseModel.DT1.FirstOrDefault());
            }
        }

        [HttpPost]
        public async Task<JsonResult> ScanPatrolQR(ScanPatrolParam scanPatrolParams)
        {
            var SiteClockIn = await _uow.UserRepository.GetClockInSiteToday(User);
            if (string.IsNullOrEmpty(SiteClockIn))
            {
                BaseResponseDto baseResponseDto = new BaseResponseDto()
                {
                    Msg = "Scan Patrol Denied",
                    Msg2 = "Please do clockin first !"
                };

                return Json(baseResponseDto);
            }

            scanPatrolParams.Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var jsonArrayParam = scanPatrolParams.ConvertToJsonArray<ScanPatrolParam>();
            var scanPatrolResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ScanPatrolQR, jsonArrayParam);
            var scanPatrolResponseModel = scanPatrolResponse.ToModelDto<ScanPatrolResponseDto>();
            if (scanPatrolResponseModel.DT2 != null)
            {
                return Json(scanPatrolResponseModel.DT2.FirstOrDefault());
            }
            else
            {
                return Json(scanPatrolResponseModel.DT1.FirstOrDefault());
            }
        }

        [HttpPost]
        public async Task<JsonResult> ScanPatrolNFC(ScanPatrolNFCParam scanPatrolParams)
        {
            var SiteClockIn = await _uow.UserRepository.GetClockInSiteToday(User);
            if (string.IsNullOrEmpty(SiteClockIn))
            {
                BaseResponseDto baseResponseDto = new BaseResponseDto()
                {
                    Msg = "Scan Patrol Denied",
                    Msg2 = "Please do clockin first !"
                };

                return Json(baseResponseDto);
            }

            scanPatrolParams.Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var jsonArrayParam = scanPatrolParams.ConvertToJsonArray<ScanPatrolNFCParam>();
            var scanPatrolResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ScanPatrolNFC, jsonArrayParam);
            var scanPatrolResponseModel = scanPatrolResponse.ToModelDto<ScanPatrolResponseDto>();
            if (scanPatrolResponseModel.DT2 != null)
            {
                return Json(scanPatrolResponseModel.DT2.FirstOrDefault());
            }
            else
            {
                return Json(scanPatrolResponseModel.DT1.FirstOrDefault());
            }
        }

        public async Task<IActionResult> ReportIncident()
        {
            var SiteClockIn = await _uow.UserRepository.GetClockInSiteToday(User);
            if (string.IsNullOrEmpty(SiteClockIn))
            {
                TempData["Function"] = string.Format("SwalAlert('{0}', '{1}', '{2}');", "Access Denied", "Access to report incident is denied , please do clockIn first", "info");

                return RedirectToAction(nameof(Index));
            }

            if (TempData["Function"] != null)
            {
                ViewBag.Function = TempData["Function"].ToString();
            }

            return View();
        }

        [RequestFormLimits(MultipartBodyLengthLimit = 40217728)]
        [RequestSizeLimit(40217728)]
        public async Task<IActionResult> SubmitIncident(ReportIncidentParam reportIncidentParams)
        {
            if (reportIncidentParams.File != null)
            {
                if (reportIncidentParams.File.Length > Convert.ToInt64(_configuration["MaxSizeFileUpload"]))
                {

                    TempData["Function"] = string.Format("SwalAlert('{0}', '{1}', '{2}');", "Report incident", "please upload file less than 30Mb", "info");

                    return RedirectToAction(nameof(ReportIncident));
                }
            }

            if (reportIncidentParams.File == null || string.IsNullOrEmpty(reportIncidentParams.Description))
            {
                TempData["Function"] = string.Format("SwalAlert('{0}', '{1}', '{2}');", "Report incident", "please upload a file and description", "info");

                return RedirectToAction(nameof(ReportIncident));
            }

            var SiteClockIn = await _uow.UserRepository.GetClockInSiteToday(User);
            reportIncidentParams.Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            _uow.ReportIncidentServices.SubmitIncident(reportIncidentParams, SiteClockIn);

            TempData["Function"] = string.Format("SwalAlert('{0}', '{1}', '{2}');", "Update incident", "incident has been reported", "info");

            return RedirectToAction(nameof(Patrol));
        }

        [HttpPost]
        public async Task<IActionResult> TakeSelfie(string image)
        {
            var imageData = Convert.FromBase64String(image.Split(',')[1]);

            SelfiePhotoUpdateParam selfiePhotoUpdateParam = new SelfiePhotoUpdateParam()
            {
                Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value,
                Date = DateTime.UtcNow.ToString("yyyy-MM-dd"),
                Photo = Convert.ToBase64String(imageData)
            };

            var takeSelfieResponse = await _uow.CommonRepository.GuardWebRequest<SelfiePhotoUpdateParam>(IFMAPIUrl.UpdateCiPhoto, selfiePhotoUpdateParam);
            var takeSelfieResponseModel = takeSelfieResponse.ToModelDto<BaseResponseDto>();

            return Json(takeSelfieResponseModel);
        }

        [AllowAnonymous]
        public IActionResult QRScanner()
        {
            if (TempData["Function"] != null)
            {
                ViewBag.Function = TempData["Function"].ToString();
            }

            return View();
        }

        [AllowAnonymous]
        public IActionResult NFCTagReader()
        {
            if (TempData["Function"] != null)
            {
                ViewBag.Function = TempData["Function"].ToString();
            }

            return View();
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> QRScanner(QRTesterParam qRTesterParam)
        {
            var jsonArrayParam = qRTesterParam.ConvertToJsonArray<QRTesterParam>();
            var takeSelfieResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.QRTester, jsonArrayParam);
            var takeSelfieResponseModel = takeSelfieResponse.ToSubmitResponseDto<BaseResponseDto>();

            return Json(takeSelfieResponseModel);
        }

        public async Task<IActionResult> SilentPatrol(string IsSkip)
        {
            var PatrolParams = new ViewPatrolParam()
            {
                Date = DateTime.UtcNow.ToSingaporeTimezone().ToString("yyyy-MM-dd"),
                Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
            };

            var PatrolResponseModel = new DataSet();

            if (IsSkip == "true")
            {
                var jsonArrayParam = PatrolParams.ConvertToJsonArray<ViewPatrolParam>();
                var PatrolResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.SkipPoint, jsonArrayParam);
                PatrolResponseModel = PatrolResponse.ToDataSetDto();
            }
            else
            {
                var jsonArrayParam = PatrolParams.ConvertToJsonArray<ViewPatrolParam>();
                var PatrolResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.NextPoint, jsonArrayParam);
                PatrolResponseModel = PatrolResponse.ToDataSetDto();
            }



            if (TempData["Function"] != null)
            {
                ViewBag.Function = TempData["Function"].ToString();
            }

            return View(PatrolResponseModel);
        }


        [HttpPost]
        public async Task<IActionResult> GetNextPoint()
        {
            var PatrolParams = new ViewPatrolParam()
            {
                Date = DateTime.UtcNow.ToSingaporeTimezone().ToString("yyyy-MM-dd"),
                //Date = "2023-06-07",
                Pers = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
            };

            var jsonArrayParam = PatrolParams.ConvertToJsonArray<ViewPatrolParam>();
            var PatrolResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.NextPoint, jsonArrayParam);

            return Json(PatrolResponse);
        }

        public async Task<IActionResult> SiteGuide()
        {
            var SiteClockIn = await _uow.UserRepository.GetClockInSiteToday(User);
            JsonArray jsonArray = new()
            {
                SiteClockIn
            };
            var SGuide = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ViewSguide, jsonArray);
            var SGuideResponseModel = SGuide.ToDataSetDto();

            if (TempData["Function"] != null)
            {
                ViewBag.Function = TempData["Function"].ToString();
            }
            return View(SGuideResponseModel);

        }

        public async Task<IActionResult> DeleteFault(int idfault)
        {
            JsonArray jsonArray = new()
            {
                idfault
            };

            var deleteFaultResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.DeleteFault, jsonArray);
            var deleteFaultResponseModel = deleteFaultResponse.ToSubmitResponseDto<BaseResponseDto>();

            TempData["Function"] = string.Format("SwalAlert('{0}', '{1}', '{2}');", deleteFaultResponseModel.Msg, deleteFaultResponseModel.Msg2, "info");

            return RedirectToAction(nameof(Patrol));

        }
    }
}
