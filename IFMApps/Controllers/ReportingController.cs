﻿using IFMApps.Extensions;
using IFMApps.Helpers;
using IFMApps.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Rotativa.AspNetCore;
using System.Security.Claims;
using System.Text.Json.Nodes;

namespace IFMApps.Controllers
{
    public class ReportingController : Controller
    {
        private readonly IUnitOfWork _uow;

        public ReportingController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [AllowAnonymous]
        public async Task<IActionResult> SiteReport(int IdSite,string Date)
        {
            JsonArray jsonArray = new()
                {
                    IdSite,
                    Date
                };
            var ViewReport = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ReportSite, jsonArray);
            var ViewReportDataSet = ViewReport.ToDataSetDto();

            return new ViewAsPdf(ViewReportDataSet);
        }

        [AllowAnonymous]
        public async Task<IActionResult> AllPointQR(int Id)
        {
            JsonArray jsonArray = new()
                {
                    Id
                };
            var ViewReport = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.PointQR2, jsonArray);
            var ViewReportDataSet = ViewReport.ToDataSetDto();

            return new ViewAsPdf(ViewReportDataSet);
        }
        [AllowAnonymous]
        public async Task<IActionResult> AllMonitorQR(int Id)
        {
            JsonArray jsonArray = new()
                {
                    Id
                };
            var ViewReport = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.MonitorQR2, jsonArray);
            var ViewReportDataSet = ViewReport.ToDataSetDto();

            return new ViewAsPdf(ViewReportDataSet);
        }

        [AllowAnonymous]
        public async Task<IActionResult> SinglePointQR(int Id)
        {
            JsonArray jsonArray = new()
                {
                    Id
                };
            var ViewReport = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.PointQR, jsonArray);
            var ViewReportDataSet = ViewReport.ToDataSetDto();

            return new ViewAsPdf(ViewReportDataSet);
        }

        [AllowAnonymous]
        public async Task<IActionResult> SingleMonitorQR(int Id)
        {
            JsonArray jsonArray = new()
                {
                    Id
                };
            var ViewReport = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.MonitorQR, jsonArray);
            var ViewReportDataSet = ViewReport.ToDataSetDto();

            return new ViewAsPdf(ViewReportDataSet);
        }

        [AllowAnonymous]
        public async Task<IActionResult> SiteReport2(int IdSite,string Periode)
        {
            JsonArray jsonArray = new()
                {
                    IdSite,
                    Periode
                };
            var ViewReport = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ReportSite2, jsonArray);
            var ViewReportDataSet = ViewReport.ToDataSetDto();

            return new ViewAsPdf(ViewReportDataSet);
        }
    }
}
