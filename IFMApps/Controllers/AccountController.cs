﻿using IFMApps.Dtos;
using IFMApps.Interface;
using IFMApps.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Text.Json.Nodes;
using IFMApps.Helpers;
using IFMApps.Extensions;

namespace IFMApps.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly IAuthServices _authService;
        public AccountController(IUnitOfWork uow, IAuthServices authService)
        {
            _authService = authService;
            _uow = uow;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (User.Identity.IsAuthenticated)
            {
                var userRole = User.FindFirst(ClaimTypes.Role)?.Value;
                if (string.IsNullOrEmpty(userRole))
                {
                    return View();
                }

                if (userRole == IFMAppRole.Guard)
                    return RedirectToAction("Index", "Pers");
                else if(userRole == IFMAppRole.Admin)
                    return RedirectToAction("Index", "Admin");
                else
                {
                    ViewBag.Function = string.Format("SwalAlert('{0}', '{1}', '{2}');", "Login Fail", "this application is not intended for your role", "info");
                    return View();
                }
            }
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginDto model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                JsonArray jsonArray = new()
                {
                    model.UserName,
                    model.Password
                };

                var user = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.SignIn, jsonArray);
                var userDto = user.ToSubmitResponseDto<LoginUserDto>();
                if (userDto.Msg == "OK")
                {
                    var claimsIdentity = _authService.AddClaims(userDto);

                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                    if (userDto.Role == IFMAppRole.Guard)
                        return RedirectToAction("Index", "Pers");
                    else if (userDto.Role == IFMAppRole.Admin)
                        return RedirectToAction("Index", "Admin");
                    else
                    {
                        ViewBag.Function = string.Format("SwalAlert('{0}', '{1}', '{2}');", "Login Fail", "this application is not intended for your role", "info");
                        return View(model);
                    }
                }
                else
                {
                    ViewBag.Function = string.Format("SwalAlert('{0}', '{1}', '{2}');", "Login Fail", userDto.Msg2, "error");
                    return View(model);
                }

            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction(nameof(Login));
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

        public IActionResult ChangePassword()
        {
            if (User.Identity.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction(nameof(Login));

            }
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(string Password)
        {
            var PersCode = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            JsonArray jsonArray = new()
                {
                    PersCode,
                    Password
                };

            var changePwd = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.ChangePassword, jsonArray);


            var changePwdModel = changePwd.ToSubmitResponseDto<BaseResponseDto>();

            ViewBag.Function = string.Format("SwalAlert('{0}', '{1}', '{2}');", changePwdModel.Msg, changePwdModel.Msg2, "info");
            return View();
        }
    }
}
