﻿using IFMApps.Dtos;
using IFMApps.Entities;
using IFMApps.Extensions;
using IFMApps.Helpers;
using IFMApps.Interface;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Text.Json.Nodes;

namespace IFMApps.Controllers
{
    public class AdminController : Controller
    {
        private readonly IUnitOfWork _uow;
        private readonly IAuthServices _authService;
        private readonly IConfiguration _configuration;

        public AdminController(IUnitOfWork uow, IAuthServices authService, IConfiguration configuration)
        {
            _configuration = configuration;
            _uow = uow;
            _authService = authService;
        }
        public async Task<IActionResult> Index(string Search = "")
        {

            if (TempData["Function"] != null)
            {
                ViewBag.Function = TempData["Function"].ToString();
            }

            if (Search != null)
            {
                ViewBag.Search = Search;
                JsonArray jsonArray = new()
                {
                    "",
                    Search
                };
                var jsonResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.FindSite2, jsonArray);
                var jsonResponseDataSet = jsonResponse.ToDataSetDto();

                return View(jsonResponseDataSet);
            }
            else
            {
                return View();
            }

        }

        public async Task<IActionResult> FindPoint(int idSite,string Search = "")
        {
            ViewBag.IdSite = idSite;
            if (TempData["Function"] != null)
            {
                ViewBag.Function = TempData["Function"].ToString();
            }

            if (Search != null)
            {

                ViewBag.Search = Search;
                JsonArray jsonArray = new()
                {
                    idSite,
                    Search
                };
                var jsonResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.FindPoint, jsonArray);
                var jsonResponseDataSet = jsonResponse.ToDataSetDto();

                return View(jsonResponseDataSet);
            }
            else
            {
                return View();
            }

        }

        [HttpPost]
        public async Task<IActionResult> UpdatePoint2(UpdatePoint2Param updatePoint2Param)
        {

            var jsonArrayParam = updatePoint2Param.ConvertToJsonArray<UpdatePoint2Param>();
            var UpdatePointResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.UpdatePoint2, jsonArrayParam);

            return Json(UpdatePointResponse.ToSubmitResponseDto<BaseResponseDto>());
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePoint3(UpdatePoint3Param updatePoint3Param)
        {

            var jsonArrayParam = updatePoint3Param.ConvertToJsonArray<UpdatePoint3Param>();
            var UpdatePointResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.UpdatePoint3, jsonArrayParam);

            return Json(UpdatePointResponse.ToSubmitResponseDto<BaseResponseDto>());
        }

        [HttpPost]
        public async Task<IActionResult> UpdateNFC(UpdateNFCParam updateNFCParam)
        {

            var jsonArrayParam = updateNFCParam.ConvertToJsonArray<UpdateNFCParam>();
            var UpdatePointResponse = await _uow.CommonRepository.GuardWebRequest(IFMAPIUrl.UpdateNFC, jsonArrayParam);

            return Json(UpdatePointResponse.ToSubmitResponseDto<BaseResponseDto>());
        }
    }
}
