﻿using IFMApps.Models;
using IFMApps.Services;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace IFMApps.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHostEnvironment _environment;
        public HomeController(ILogger<HomeController> logger,IHostEnvironment environment)
        {
            _environment = environment;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        public IActionResult FirebaseConfig()
        {
            return View();
        }

        public IActionResult SubscribeNotification()
        {
            FirebaseServices firebaseServices = new FirebaseServices();
            firebaseServices.SubscribeTopic();
            return View(nameof(Privacy));
        }

        public IActionResult UnsubscribeNotification()
        {
            FirebaseServices firebaseServices = new FirebaseServices();
            firebaseServices.UnSubscribeTopic();
            return View(nameof(Privacy));
        }
    }
}