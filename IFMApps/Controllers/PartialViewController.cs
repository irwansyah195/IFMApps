﻿using Microsoft.AspNetCore.Mvc;

namespace IFMApps.Controllers
{
    public class PartialViewController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult PageSearchCommon(string Action, string Controller)
        {
            ViewBag.Action = Action;
            ViewBag.Controller = Controller;
            return View();
        }
    }
}
