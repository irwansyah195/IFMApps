﻿using IFMApps.Helpers;

namespace IFMApps.Interface
{
    public interface IReportRepository
    {
        void SaveHttpResponseAsFile(string RequestUrl, string FilePath, dynamic Parameter);
        SiteReportResponse SaveAndUploadSiteReport(int IdSite,DateTime Periode);

        Task<string> GenerateSiteReport();

        string SaveHttpResponseAsBase64(string RequestUrl, dynamic Parameter);
    }
}
