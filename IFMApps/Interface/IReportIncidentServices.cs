﻿using IFMApps.Helpers;

namespace IFMApps.Interface
{
    public interface IReportIncidentServices
    {
        string UploadIncidentToAWS(string Site, string FilePath, string Extension);

        void SubmitIncident(ReportIncidentParam reportIncidentParams, string Site);
    }
}
