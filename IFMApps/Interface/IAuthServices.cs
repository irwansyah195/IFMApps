﻿using IFMApps.Dtos;
using System.Security.Claims;

namespace IFMApps.Interface
{
    public interface IAuthServices
    {
        ClaimsIdentity AddClaims(LoginUserDto authUser);
        ClaimsIdentity AddSiteToClaims(ClaimsPrincipal existingClaims, string Site);
    }
}
