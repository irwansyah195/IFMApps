﻿using System.Text.Json.Nodes;

namespace IFMApps.Interface
{
    public interface ICommonRepository
    {
        Task<string> GuardWebRequest(string Url, JsonArray parameter);
        Task<string> GuardWebRequest<T>(string Url, T parameter);

        string GetAppSettingJson(string Section);
    }
}
