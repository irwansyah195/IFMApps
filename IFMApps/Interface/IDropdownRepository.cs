﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace IFMApps.Interface
{
    public interface IDropdownRepository
    {
        Task<SelectList> GetDrpItemOrganizations(string SelectedValue);
        Task<SelectList> GetDrpItemSites(string SelectedValue);
        List<SelectListItem> GetDrpItemPoint(string SelectedValue);
        List<SelectListItem> GetDrpGuards(string SelectedValue);
        List<SelectListItem> GetDrpFieldValue(string fieldType, string SelectedValue);
    }
}
