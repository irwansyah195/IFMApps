﻿using System.Security.Claims;

namespace IFMApps.Interface
{
    public interface IUserRepository
    {
        Task<string> GetClockInSiteToday(ClaimsPrincipal existingClaims);
    }
}
