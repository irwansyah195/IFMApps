﻿namespace IFMApps.Interface
{
    public interface IAWSServices
    {
        string UploadFileToS3(Stream stream, string subDirectoryInBucket, string fileNameInS3);
    }
}
