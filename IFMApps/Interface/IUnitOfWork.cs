﻿namespace IFMApps.Interface
{
    public interface IUnitOfWork
    {
        ICommonRepository CommonRepository { get; }
        IDropdownRepository DropdownRepository { get; }
        IUserRepository UserRepository { get; }
        IReportIncidentServices ReportIncidentServices { get; }

        IReportRepository ReportRepository { get; }
    }
}
