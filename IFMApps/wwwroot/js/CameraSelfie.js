﻿const videoElement = document.getElementById('videoElement');
const captureButton = document.getElementById('captureButton');
let mediaStream = null;
// Access the camera and stream video to the video element
function accesCamera() {
    navigator.mediaDevices.getUserMedia({ video: true })
        .then((stream) => {
            videoElement.srcObject = stream;
            mediaStream = stream;
        })
        .catch((err) => {
            console.log(err);
        });
}

function stopAccessCamera() {
    if (mediaStream) {
        mediaStream.getTracks().forEach((track) => {
            track.stop();
        });
        videoElement.srcObject = null;
        mediaStream = null;
    }
}

