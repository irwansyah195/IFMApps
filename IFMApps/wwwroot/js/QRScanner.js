﻿
const html5QrCode = new Html5Qrcode(
    "reader", { formatsToSupport: [Html5QrcodeSupportedFormats.QR_CODE] });

const config = { fps: 120, qrbox: { width: 250, height: 250 } };

function onScanError(errorMessage) {
    SwalAlert('Fail QR Scanning', errorMessage, 'error');
}

function removeScanWithFile() {
    $("#html5-qrcode-anchor-scan-type-change").text("");
}


