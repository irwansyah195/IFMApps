﻿function reverseGeocode(lon, lat) {
    fetch('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lon=' + lon + '&lat=' + lat)
        .then(function (response) {
            return response.json();
        }).then(function (json) {
            console.log(json);
        });
}


function GetLocation() {
    map = new OpenLayers.Map("googleMap");
    var mapnik = new OpenLayers.Layer.OSM();
    map.addLayer(mapnik);

    var geolocate = new OpenLayers.Control.Geolocate({
        bind: false,
        geolocationOptions: {
            enableHighAccuracy: false,
            maximumAge: 0,
            timeout: 5000
        }
    });
    map.addControl(geolocate);
    geolocate.activate();

    geolocate.events.register("locationupdated", geolocate, function (e) {
        var longitude = e.position.coords.longitude;
        var latitude = e.position.coords.latitude;
        reverseGeocode(longitude, latitude);
        console.log(longitude)
        console.log(latitude)
        $("#Latitude").val(latitude);
        $("#Longitude").val(longitude);
    })

    geolocate.events.register("locationfailed", this, function (e) {
        console.log(e)
        sweetAlert("Get Location Failed !!", e.error.message, "error");

    });




}