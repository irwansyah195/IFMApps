﻿function reverseGeocode(lon, lat) {
    fetch('https://nominatim.openstreetmap.org/reverse?format=jsonv2&lon=' + lon + '&lat=' + lat)
        .then(function (response) {
            return response.json();
        }).then(function (json) {
            console.log(json);
        });
}


function GetLocation(latp, lngp) {

    var zoom = 16;
    var fromProjection = new OpenLayers.Projection("EPSG:4326");
    var toProjection = new OpenLayers.Projection("EPSG:900913");

    map = new OpenLayers.Map("googleMap");
    var mapnik = new OpenLayers.Layer.OSM();
    map.addLayer(mapnik);

    var geolocate = new OpenLayers.Control.Geolocate({
        bind: false,
        geolocationOptions: {
            enableHighAccuracy: true,
            maximumAge: 0,
            timeout: 7000
        }
    });
    map.addControl(geolocate);
    geolocate.activate();

    geolocate.events.register("locationupdated", geolocate, function (e) {
        var longitude = e.position.coords.longitude;
        var latitude = e.position.coords.latitude;
        var positionCurrent = new OpenLayers.LonLat(longitude, latitude).transform(fromProjection, toProjection);
        var positionPoint = new OpenLayers.LonLat(lngp, latp).transform(fromProjection, toProjection);
        var markers = new OpenLayers.Layer.Markers("Markers");
        map.addLayer(markers);
        markers.addMarker(new OpenLayers.Marker(positionCurrent));
        markers.addMarker(new OpenLayers.Marker(positionPoint));

        map.setCenter(positionCurrent, zoom);
        reverseGeocode(lngp, latp);
        console.log(lngp)
        console.log(latp)
        $("#Latitude").val(latitude);
        $("#Longitude").val(longitude);
    })

    geolocate.events.register("locationfailed", this, function (e) {
        console.log(e)
        sweetAlert("Get Location Failed !!", e.error.message, "error");

    });




}