﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
function SwalAlert(Title, Message, Type) {
    swal({
        title: Title,
        text: Message,
        icon: Type
    });
}

function SwalAlertLocationReload(Title, Message, Type) {
    swal({
        title: Title,
        text: Message,
        icon: Type
    }).then((value) => {
        location.reload();
    });;
}

$(".datepickerMonth").datepicker({
    format: 'yyyy-mm',
    autoclose: true,
    viewMode: "months",
    minViewMode: "months"
});

$(".datepicker").datepicker({
    format: 'yyyy-mm-dd',
    autoclose: true
});

$(".timePicker").datetimepicker({
    format: 'HH:mm'
});
$(".timeSecondPicker").datetimepicker({
    format: 'HH:mm:ss'
});

$(".select2").select2();

// update the file name display when a file is selected
$(document).on('change', '.attachment-input input[type=file]', function () {
    var fileName = $(this).val().split('\\').pop();
    if (fileName) {
        $(this).siblings('.attachment-file-name').html(fileName);
    } else {
        $(this).siblings('.attachment-file-name').html('No file chosen');
    }
});


function GetDateFormat() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + '-' + mm + '-' + dd;
    return today;
}

function GetDateTimeFormat() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0! 
    var hh = today.getHours();
    var min = today.getMinutes();
    var ss = today.getSeconds();
    var yyyy = today.getFullYear();

    todaywithTime = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + ss;
    return todaywithTime;
}

function GetTimeFormat() {
    var today = new Date();

    var hh = today.getHours();
    var min = today.getMinutes();
    var ss = today.getSeconds();

    todaywithTime = hh + ':' + min + ':' + ss;
    return todaywithTime;
}

$('form').submit(function (e) {
    setVisible('#loading', true, e);
})