﻿using IFMApps.Dtos;
using IFMApps.Interface;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

namespace IFMApps.Services
{
    public class AuthServices : IAuthServices
    {
        public ClaimsIdentity AddClaims(LoginUserDto authUser)
        {
            var claims = new List<Claim>
            {
                    new Claim(ClaimTypes.Name, authUser.Name),
                    new Claim(ClaimTypes.Role, authUser.Role),
                    new Claim(ClaimTypes.NameIdentifier,authUser.Code),
                    new Claim("Welcome",authUser.Welcome),
                    new Claim("IdOrg",authUser.idorg.ToString()),
                    new Claim("IdUser",authUser.Iduser.ToString())
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

            return claimsIdentity;
        }

        public ClaimsIdentity AddSiteToClaims(ClaimsPrincipal existingClaims, string Site)
        {
            var claims = new List<Claim>
            {
                    new Claim(ClaimTypes.Name, existingClaims.FindFirst(ClaimTypes.Name)?.Value),
                    new Claim(ClaimTypes.Role, existingClaims.FindFirst(ClaimTypes.Role)?.Value),
                    new Claim(ClaimTypes.NameIdentifier,existingClaims.FindFirst(ClaimTypes.NameIdentifier)?.Value),
                    new Claim("Welcome",existingClaims.FindFirst("Welcome")?.Value),
                    new Claim("IdOrg",existingClaims.FindFirst("IdOrg")?.Value),
                    new Claim("IdUser",existingClaims.FindFirst("IdUser")?.Value),
                    new Claim("Site",Site)
            };

            var updatedClaimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            return updatedClaimsIdentity;
        }
    }
}
