﻿using Amazon.S3.Transfer;
using Amazon.S3;
using IFMApps.Interface;
using Amazon;

namespace IFMApps.Services
{
    public class AWSServices : IAWSServices
    {
        private readonly RegionEndpoint _bucketRegion = RegionEndpoint.APSoutheast1;
        private readonly string _bucketName;
        private readonly string _AWSAccessKey;
        private readonly string _AWSSecretKey;

        private readonly IAmazonS3 _client;
        private readonly IConfiguration _configuration;

        public AWSServices(IConfiguration configuration)
        {
            _configuration = configuration;
            _bucketName = _configuration.GetValue<string>("S3:BucketName");
            _AWSAccessKey = _configuration.GetValue<string>("S3:AWSAccessKey");
            _AWSSecretKey = _configuration.GetValue<string>("S3:AWSSecretKey");
            _client = new AmazonS3Client(_AWSAccessKey, _AWSSecretKey, _bucketRegion);
        }

        public string UploadFileToS3(Stream stream, string subDirectoryInBucket, string fileNameInS3)
        {
            try
            {
                var utility = new TransferUtility(_client);
                var request = new TransferUtilityUploadRequest();

                if (string.IsNullOrEmpty(subDirectoryInBucket) || subDirectoryInBucket is null)
                {
                    request.BucketName = _bucketName;
                }
                else
                {
                    request.BucketName = _bucketName + "/" + subDirectoryInBucket;
                }

                request.Key = fileNameInS3;
                request.CannedACL = S3CannedACL.PublicRead;
                request.InputStream = stream;
                utility.Upload(request);

                string publicUrl = "https://" + _bucketName + "/" + subDirectoryInBucket + "/" + fileNameInS3;

                return publicUrl;
            }
            catch (AmazonS3Exception exS3)
            {
                return "[exS3]: " + exS3.Message;
            }
            catch (Exception ex)
            {
                return "[ex]: " + ex.Message;
            }
        }
    }
}
