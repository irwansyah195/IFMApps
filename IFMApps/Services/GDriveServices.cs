﻿using Google;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Drive.v3;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Microsoft.AspNetCore.StaticFiles;
using System.Net;
using System.Net.Mime;

namespace IFMApps.Services
{
    public class GDriveServices
    {
        private DriveService _driveService;
        private readonly IHostEnvironment _environment;
        public GDriveServices(IHostEnvironment environment)
        {
            _environment = environment;
            string CredentialsPath = Path.Combine(_environment.ContentRootPath, "wwwroot", "google_credentials.json");
            var credential = GoogleCredential.FromFile(CredentialsPath).CreateScoped(DriveService.ScopeConstants.Drive);


            //Create the Drive service
            _driveService = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "IFMGDrive"
            });
        }

        public string SaveFile(string filePath, string fileName)
        {
            FilesResource.CreateMediaUpload request;

            try
            {
                var fileMetadata = new Google.Apis.Drive.v3.Data.File()
                {
                    Name = fileName,
                    Parents = new[] { "1tz_Z_qz-_KBCvRJQGXidmCq7YnuQbxip" }
                };
                
                string contentType;
                new FileExtensionContentTypeProvider().TryGetContentType(fileName, out contentType);

                if (filePath.Contains("http"))
                {

                    var req = System.Net.WebRequest.Create(filePath);
                    using (Stream stream = req.GetResponse().GetResponseStream())
                    {
                        request = _driveService.Files.Create(fileMetadata, stream, contentType);
                        request.Fields = "*";
                        var response = request.Upload();
                    }
                }
                else
                {
                    using (var stream = new FileStream(filePath, FileMode.Open))
                    {
                        request = _driveService.Files.Create(fileMetadata, stream, contentType);
                        request.Fields = "*";
                        var response = request.Upload();
                    }
                }

                return request.ResponseBody.WebViewLink;

            }
            catch (Exception ex)
            {
                throw;
            }
        }

    }
}
