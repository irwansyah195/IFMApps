﻿using IFMApps.Data;
using IFMApps.Interface;
using Quartz;
using Serilog;

namespace IFMApps.Services
{
    public class QuartzScheduler : IJob
    {
        private readonly IConfiguration _configuration;
        private readonly IHostEnvironment _environment;
        public QuartzScheduler(IConfiguration configuration, IHostEnvironment environment)
        {
            _configuration = configuration;
            _environment = environment;
        }
        public async Task Execute(IJobExecutionContext context)
        {
            var key = context.JobDetail.Key.ToString().Replace("DEFAULT.", "");


            try
            {
                Log.Information("Job is started");
                switch (key)
                {
                    case "SiteReportSchedule":
                        ReportRepository reportRepository = new ReportRepository(_configuration, _environment);
                        var Result = await reportRepository.GenerateSiteReport();
                        Log.Information("Monthly site report : " + Result);
                        //Log.Information("Monthly site report jalan");
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex.ToString(), ex);
            }
        }
    }
}
