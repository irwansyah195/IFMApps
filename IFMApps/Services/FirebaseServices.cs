﻿using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Serilog;

namespace IFMApps.Services
{
    public class FirebaseServices
    {
        public async void SubscribeTopic()
        {
            // These registration tokens come from the client FCM SDKs.
            var registrationTokens = new List<string>()
            {
                "ejRZymWA1i2e4RKFdXbjb4:APA91bH7wWUlXcaGW3AnnG3c43jL2ZbbY7Uy3-uYzPcooDvGuqkUUf_tJuQ2MPJei-7GrYVD32wHu8jqVv0J5AV3IIk_dD21_5LKwjSv46linT7IOiTvK_M4BE73Fj-DHfJjeCNZfBVi",
            };

            // Subscribe the devices corresponding to the registration tokens to the
            // topic
            var response = await FirebaseMessaging.DefaultInstance.SubscribeToTopicAsync(
                registrationTokens, "topikk");
            // See the TopicManagementResponse reference documentation
            // for the contents of response.
            Log.Information($"{response.SuccessCount} tokens were subscribed successfully");
        }

        public async void UnSubscribeTopic()
        {
            // These registration tokens come from the client FCM SDKs.
            var registrationTokens = new List<string>()
            {
                "ejRZymWA1i2e4RKFdXbjb4:APA91bH7wWUlXcaGW3AnnG3c43jL2ZbbY7Uy3-uYzPcooDvGuqkUUf_tJuQ2MPJei-7GrYVD32wHu8jqVv0J5AV3IIk_dD21_5LKwjSv46linT7IOiTvK_M4BE73Fj-DHfJjeCNZfBVi",
            };

            // Subscribe the devices corresponding to the registration tokens to the
            // topic
            var response = await FirebaseMessaging.DefaultInstance.SubscribeToTopicAsync(
                registrationTokens, "topikk");
            // See the TopicManagementResponse reference documentation
            // for the contents of response.
            Log.Information($"{response.SuccessCount} tokens were unsubscribed successfully");

        }
    }
}
