﻿using IFMApps.Data;
using IFMApps.Extensions;
using IFMApps.Helpers;
using IFMApps.Interface;
using Serilog;
using Xabe.FFmpeg;

namespace IFMApps.Services
{
    public class ReportIncidentServices : IReportIncidentServices
    {
        private readonly IConfiguration _configuration;
        private readonly IHostEnvironment _environment;

        public ReportIncidentServices(IConfiguration configuration, IHostEnvironment environment)
        {
            _environment = environment;
            _configuration = configuration;
        }
        public async void SubmitIncident(ReportIncidentParam reportIncidentParams, string Site)
        {
            string AWSUrl = string.Empty;
            string RequestID = DateTime.UtcNow.ToSingaporeTimezone().ToString("ddMMyyyyHHmmssfff");
            string Extension = Path.GetExtension(reportIncidentParams.File.FileName);
            string TempOutputFolder = Path.Combine(_environment.ContentRootPath, "Temp");

            string FileName = "Incident_" + RequestID + Extension;
            string FullPath = Path.Combine(TempOutputFolder, FileName);


            if (!Directory.Exists(TempOutputFolder))
            {
                Directory.CreateDirectory(TempOutputFolder);
            }

            using (Stream responseStream = reportIncidentParams.File.OpenReadStream())
            {
                Stream FinalStream = responseStream;

                using (var fileStream = File.Create(FullPath))
                {
                    FinalStream.CopyTo(fileStream);
                }

                responseStream.Close();
                FinalStream.Close();
            }

            string FileNameNew = "Incident_Conversion_" + RequestID + Extension;
            string FullPathNew = Path.Combine(TempOutputFolder, FileNameNew);

            if (reportIncidentParams.File.ContentType.StartsWith("video/"))
            {
                var FFmpegExecPath = Path.Combine(_environment.ContentRootPath, "wwwroot", "ffmpeg");
                FFmpeg.SetExecutablesPath(FFmpegExecPath);

                var MediaInfo = await FFmpeg.GetMediaInfo(FullPath);

                var audioStream = MediaInfo.AudioStreams.First()
                    .SetCodec(AudioCodec.aac)
                    .SetBitrate(128000);

                var videoStream = MediaInfo.VideoStreams.First()
                            .SetCodec(VideoCodec.h264);

                //var WidthResize = videoStream.Width / 2;
                //var HeightResize = videoStream.Height / 2;

                //videoStream.SetSize(WidthResize, HeightResize);

                await FFmpeg.Conversions.New()
                    .AddStream(videoStream)
                    .AddStream(audioStream)
                    .SetOutput(FullPathNew)
                    .Start();

                if (File.Exists(FullPathNew))
                {
                    AWSUrl = UploadIncidentToAWS(Site, FullPathNew, Extension);
                }
            }
            else
            {
                AWSUrl = UploadIncidentToAWS(Site, FullPath, Extension);
            }


            ReportIncidentParamsWebRequest reportIncidentParamsWebRequest = new ReportIncidentParamsWebRequest()
            {
                Pers = reportIncidentParams.Pers,
                Description = reportIncidentParams.Description,
                Site = Site,
                Url = AWSUrl
            };

            var jsonArrayParam = reportIncidentParamsWebRequest.ConvertToJsonArray<ReportIncidentParamsWebRequest>();
            CommonRepository commonRepository = new CommonRepository(_configuration);
            var UpdateIncident = await commonRepository.GuardWebRequest(IFMAPIUrl.ReportFault2, jsonArrayParam);

            File.Delete(FullPathNew);
            File.Delete(FullPath);

            Log.Information(UpdateIncident);
        }

        public string UploadIncidentToAWS(string Site, string FilePath, string Extension)
        {
            AWSServices aWSServices = new AWSServices(_configuration);
            string RequestId = DateTime.UtcNow.ToSingaporeTimezone().ToString("ddMMyyyyHHmmssfff");
            string response = string.Empty;

            using (Stream filestream = File.Open(FilePath, FileMode.Open))
            {
                string s3DirectoryName = "T99";
                string s3FileName = Site + "_IFMFault_" + RequestId + Extension;
                //response = "http";
                response = aWSServices.UploadFileToS3(filestream, s3DirectoryName, s3FileName);
            }

            if (response.StartsWith("http"))
            {
                return response;
            }
            else
            {
                throw new Exception(response);
            }
        }
    }
}