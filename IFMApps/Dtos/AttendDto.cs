﻿namespace IFMApps.Dtos
{
    public class AttendDto
    {
        public string Pers { get; set; }
        public string Timein { get; set; }
        public string Timeout { get; set; }
        public string idroute { get; set; }
        public string Label { get; set; }
    }
}
