﻿namespace IFMApps.Dtos
{
    public class LoginUserDto : BaseResponseDto
    {
        public int Iduser { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public int idorg { get; set; }
        public string Welcome { get; set; }
    }
}
