﻿namespace IFMApps.Dtos
{
    public class SubmitResponseDto<T>
    {
        public List<T> DT1 { get; set; }
    }
}
