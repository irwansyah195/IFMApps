﻿using System.ComponentModel.DataAnnotations;

namespace IFMApps.Dtos
{
    public class LoginDto
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
