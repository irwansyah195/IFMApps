﻿using IFMApps.Helpers;

namespace IFMApps.Dtos
{
    public class ScanPatrolResponseDto
    {
        public List<ScanPatrolResponse> DT1 { get; set; }
        public List<ScanPatrolResponse2> DT2 { get; set; }
    }
}
