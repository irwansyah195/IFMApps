﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Reading
{
    public int Idreading { get; set; }

    public int Idmeter { get; set; }

    public DateOnly? Date { get; set; }

    public int Reading1 { get; set; }

    public int Consump { get; set; }

    public decimal? Var { get; set; }

    public string Status { get; set; }
}
