﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class User
{
    public int Iduser { get; set; }

    public int Idorg { get; set; }

    public string Code { get; set; }

    public string Name { get; set; }

    public string Email { get; set; }

    public string Telephone { get; set; }

    public string Password { get; set; }

    public string Role { get; set; }

    public DateOnly? Date { get; set; }

    public string Status { get; set; }

    public int Cnt { get; set; }

    public DateOnly? Access { get; set; }
}
