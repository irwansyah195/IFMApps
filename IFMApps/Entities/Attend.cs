﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Attend
{
    public int Idattend { get; set; }

    public DateOnly? Date { get; set; }

    public int Idsite { get; set; }

    public string Pers { get; set; }

    public string Shift { get; set; }

    public TimeOnly? Timein { get; set; }

    public TimeOnly? Timeout { get; set; }

    public int Idroute { get; set; }

    public short Scan { get; set; }

    public short Miss { get; set; }

    public short Fault { get; set; }

    public decimal Workhr { get; set; }

    public decimal Pay { get; set; }
}
