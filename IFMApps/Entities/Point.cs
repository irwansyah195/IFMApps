﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Point
{
    public int Idpoint { get; set; }

    public int? Idsite { get; set; }

    public string Name { get; set; }

    public float? Lng { get; set; }

    public float? Lat { get; set; }
}
