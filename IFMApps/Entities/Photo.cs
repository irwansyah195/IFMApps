﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Photo
{
    public int Idphoto { get; set; }

    public int Idsite { get; set; }

    public short Channel { get; set; }

    public short Preset { get; set; }

    public DateTime? Datetime { get; set; }

    public string Photo1 { get; set; }

    public decimal Var { get; set; }

    public string Status { get; set; }
}
