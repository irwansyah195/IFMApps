﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Guide
{
    public int Idguide { get; set; }

    public DateOnly? Date { get; set; }

    public string Title { get; set; }

    public string Url { get; set; }
}
