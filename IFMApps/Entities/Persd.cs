﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Persd
{
    public int Idpersd { get; set; }

    public int? Idpers { get; set; }

    public int? Idsite { get; set; }
}
