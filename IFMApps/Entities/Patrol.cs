﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Patrol
{
    public int Idpatrol { get; set; }

    public DateOnly? Date { get; set; }

    public int? Idpoint { get; set; }

    public string Pers { get; set; }

    public TimeOnly? Time { get; set; }

    public TimeOnly? Scan { get; set; }
}
