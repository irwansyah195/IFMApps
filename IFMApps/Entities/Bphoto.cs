﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Bphoto
{
    public int Idphoto { get; set; }

    public int? Idsite { get; set; }

    public short? Channel { get; set; }

    public short? Preset { get; set; }

    public string Photo { get; set; }
}
