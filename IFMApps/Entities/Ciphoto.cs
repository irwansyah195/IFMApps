﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Ciphoto
{
    public int Idciphoto { get; set; }

    public DateOnly? Date { get; set; }

    public string Pers { get; set; }

    public byte[] Photo { get; set; }
}
