﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Screenshot
{
    public int Idscreenshot { get; set; }

    public DateTime? Datetime { get; set; }

    public string Location { get; set; }

    public byte[] Screenshot1 { get; set; }
}
