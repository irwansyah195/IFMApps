﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Incident
{
    public int Idincident { get; set; }

    public int? Idsite { get; set; }

    public string Pers { get; set; }

    public DateTime? Datetime { get; set; }

    public string Url { get; set; }

    public float? Lng { get; set; }

    public float? Lat { get; set; }

    public string Descr { get; set; }

    public string Status { get; set; }
}
