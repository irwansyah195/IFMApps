﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Gp
{
    public int Idgps { get; set; }

    public DateTime? Date { get; set; }

    public string Qr { get; set; }

    public float? Lng { get; set; }

    public float? Lat { get; set; }

    public string Pers { get; set; }
}
