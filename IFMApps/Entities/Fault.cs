﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Fault
{
    public int Idfault { get; set; }

    public int? Idsite { get; set; }

    public int Id { get; set; }

    public string Location { get; set; }

    public DateTime? Datetime { get; set; }

    public string Telephone { get; set; }

    public string Name { get; set; }

    public string Url { get; set; }

    public string Descr { get; set; }

    public string Status { get; set; }

    public string Pers { get; set; }
}
