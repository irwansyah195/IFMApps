﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Route
{
    public int Idroute { get; set; }

    public int? Idsite { get; set; }

    public string Descr { get; set; }
}
