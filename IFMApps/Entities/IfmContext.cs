﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace IFMApps.Entities;

public partial class IfmContext : DbContext
{
    public IfmContext()
    {
    }

    public IfmContext(DbContextOptions<IfmContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Attend> Attends { get; set; }

    public virtual DbSet<Bphoto> Bphotos { get; set; }

    public virtual DbSet<Camera> Cameras { get; set; }

    public virtual DbSet<Ciphoto> Ciphotos { get; set; }

    public virtual DbSet<Fault> Faults { get; set; }

    public virtual DbSet<FieldSpec> FieldSpecs { get; set; }

    public virtual DbSet<FieldValue> FieldValues { get; set; }

    public virtual DbSet<Gp> Gps { get; set; }

    public virtual DbSet<Guide> Guides { get; set; }

    public virtual DbSet<Incident> Incidents { get; set; }

    public virtual DbSet<Leave> Leaves { get; set; }

    public virtual DbSet<Menu> Menus { get; set; }

    public virtual DbSet<Meter> Meters { get; set; }

    public virtual DbSet<Monitor> Monitors { get; set; }

    public virtual DbSet<Org> Orgs { get; set; }

    public virtual DbSet<Patrol> Patrols { get; set; }

    public virtual DbSet<Per> Pers { get; set; }

    public virtual DbSet<Persd> Persds { get; set; }

    public virtual DbSet<Photo> Photos { get; set; }

    public virtual DbSet<Point> Points { get; set; }

    public virtual DbSet<Reading> Readings { get; set; }

    public virtual DbSet<Relief> Reliefs { get; set; }

    public virtual DbSet<Route> Routes { get; set; }

    public virtual DbSet<Routep> Routeps { get; set; }

    public virtual DbSet<Screenshot> Screenshots { get; set; }

    public virtual DbSet<Shift> Shifts { get; set; }

    public virtual DbSet<Site> Sites { get; set; }

    public virtual DbSet<SiteRept> SiteRepts { get; set; }

    public virtual DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseMySql("server=my.tklcloud.com;port=3306;database=IFM;user id=guard;password=Guard9729#;charset=utf8", Microsoft.EntityFrameworkCore.ServerVersion.Parse("5.6.50-mysql"));

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .UseCollation("utf8_general_ci")
            .HasCharSet("utf8");

        modelBuilder.Entity<Attend>(entity =>
        {
            entity.HasKey(e => e.Idattend).HasName("PRIMARY");

            entity.ToTable("attend");

            entity.Property(e => e.Idattend)
                .HasColumnType("int(11)")
                .HasColumnName("idattend");
            entity.Property(e => e.Date).HasColumnName("date");
            entity.Property(e => e.Fault)
                .HasColumnType("smallint(6)")
                .HasColumnName("fault");
            entity.Property(e => e.Idroute)
                .HasColumnType("int(11)")
                .HasColumnName("idroute");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Miss)
                .HasColumnType("smallint(6)")
                .HasColumnName("miss");
            entity.Property(e => e.Pay)
                .HasPrecision(6, 1)
                .HasColumnName("pay");
            entity.Property(e => e.Pers)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("pers");
            entity.Property(e => e.Scan)
                .HasColumnType("smallint(6)")
                .HasColumnName("scan");
            entity.Property(e => e.Shift)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("shift");
            entity.Property(e => e.Timein)
                .HasColumnType("time")
                .HasColumnName("timein");
            entity.Property(e => e.Timeout)
                .HasColumnType("time")
                .HasColumnName("timeout");
            entity.Property(e => e.Workhr)
                .HasPrecision(6, 1)
                .HasColumnName("workhr");
        });

        modelBuilder.Entity<Bphoto>(entity =>
        {
            entity.HasKey(e => e.Idphoto).HasName("PRIMARY");

            entity.ToTable("bphoto");

            entity.Property(e => e.Idphoto)
                .HasColumnType("int(11)")
                .HasColumnName("idphoto");
            entity.Property(e => e.Channel)
                .HasColumnType("smallint(6)")
                .HasColumnName("channel");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Photo)
                .HasMaxLength(256)
                .HasColumnName("photo");
            entity.Property(e => e.Preset)
                .HasColumnType("smallint(6)")
                .HasColumnName("preset");
        });

        modelBuilder.Entity<Camera>(entity =>
        {
            entity.HasKey(e => e.Idcamera).HasName("PRIMARY");

            entity.ToTable("camera");

            entity.Property(e => e.Idcamera)
                .HasColumnType("int(11)")
                .HasColumnName("idcamera");
            entity.Property(e => e.Channel)
                .HasColumnType("smallint(6)")
                .HasColumnName("channel");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Preset)
                .HasColumnType("smallint(6)")
                .HasColumnName("preset");
        });

        modelBuilder.Entity<Ciphoto>(entity =>
        {
            entity.HasKey(e => e.Idciphoto).HasName("PRIMARY");

            entity.ToTable("ciphoto");

            entity.Property(e => e.Idciphoto)
                .HasColumnType("int(11)")
                .HasColumnName("idciphoto");
            entity.Property(e => e.Date).HasColumnName("date");
            entity.Property(e => e.Pers)
                .HasMaxLength(45)
                .HasColumnName("pers");
            entity.Property(e => e.Photo).HasColumnName("photo");
        });

        modelBuilder.Entity<Fault>(entity =>
        {
            entity.HasKey(e => e.Idfault).HasName("PRIMARY");

            entity.ToTable("fault");

            entity.Property(e => e.Idfault)
                .HasColumnType("int(11)")
                .HasColumnName("idfault");
            entity.Property(e => e.Datetime)
                .HasColumnType("datetime")
                .HasColumnName("datetime");
            entity.Property(e => e.Descr)
                .IsRequired()
                .HasMaxLength(500)
                .HasColumnName("descr");
            entity.Property(e => e.Id)
                .HasColumnType("int(11)")
                .HasColumnName("id");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Location)
                .HasMaxLength(45)
                .HasColumnName("location");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.Pers)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("pers");
            entity.Property(e => e.Status)
                .IsRequired()
                .HasMaxLength(12)
                .HasColumnName("status");
            entity.Property(e => e.Telephone)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("telephone");
            entity.Property(e => e.Url)
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnName("url");
        });

        modelBuilder.Entity<FieldSpec>(entity =>
        {
            entity.HasKey(e => e.IdfieldSpec).HasName("PRIMARY");

            entity.ToTable("field_spec");

            entity.Property(e => e.IdfieldSpec)
                .HasColumnType("int(11)")
                .HasColumnName("idfield_spec");
            entity.Property(e => e.Datasource)
                .HasMaxLength(128)
                .HasColumnName("datasource");
            entity.Property(e => e.IdtableSpec)
                .HasColumnType("int(11)")
                .HasColumnName("idtable_spec");
            entity.Property(e => e.Info)
                .HasMaxLength(128)
                .HasColumnName("info");
            entity.Property(e => e.Label)
                .HasMaxLength(128)
                .HasColumnName("label");
            entity.Property(e => e.Length)
                .HasColumnType("smallint(6)")
                .HasColumnName("length");
            entity.Property(e => e.MaxChoice)
                .HasColumnType("smallint(6)")
                .HasColumnName("max_choice");
            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.Required)
                .HasDefaultValueSql("b'0'")
                .HasColumnType("bit(1)")
                .HasColumnName("required");
            entity.Property(e => e.Type)
                .HasMaxLength(2)
                .IsFixedLength()
                .HasColumnName("type");
        });

        modelBuilder.Entity<FieldValue>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("PRIMARY");

            entity.ToTable("field_value");

            entity.Property(e => e.Id)
                .HasColumnType("int(11)")
                .HasColumnName("id");
            entity.Property(e => e.Descr)
                .HasMaxLength(45)
                .HasColumnName("descr");
            entity.Property(e => e.Field)
                .HasMaxLength(45)
                .HasColumnName("field");
            entity.Property(e => e.Value)
                .HasMaxLength(45)
                .HasColumnName("value");
        });

        modelBuilder.Entity<Gp>(entity =>
        {
            entity.HasKey(e => e.Idgps).HasName("PRIMARY");

            entity.ToTable("gps");

            entity.Property(e => e.Idgps)
                .HasColumnType("int(11)")
                .HasColumnName("idgps");
            entity.Property(e => e.Date)
                .HasColumnType("datetime")
                .HasColumnName("date");
            entity.Property(e => e.Lat)
                .HasColumnType("float(9,6)")
                .HasColumnName("lat");
            entity.Property(e => e.Lng)
                .HasColumnType("float(9,6)")
                .HasColumnName("lng");
            entity.Property(e => e.Pers)
                .HasMaxLength(45)
                .HasColumnName("pers");
            entity.Property(e => e.Qr)
                .HasMaxLength(45)
                .HasColumnName("qr");
        });

        modelBuilder.Entity<Guide>(entity =>
        {
            entity.HasKey(e => e.Idguide).HasName("PRIMARY");

            entity.ToTable("guide");

            entity.Property(e => e.Idguide)
                .HasColumnType("int(11)")
                .HasColumnName("idguide");
            entity.Property(e => e.Date).HasColumnName("date");
            entity.Property(e => e.Title)
                .HasMaxLength(64)
                .HasColumnName("title");
            entity.Property(e => e.Url)
                .HasMaxLength(128)
                .HasColumnName("url");
        });

        modelBuilder.Entity<Incident>(entity =>
        {
            entity.HasKey(e => e.Idincident).HasName("PRIMARY");

            entity.ToTable("incident");

            entity.Property(e => e.Idincident)
                .HasColumnType("int(11)")
                .HasColumnName("idincident");
            entity.Property(e => e.Datetime)
                .HasColumnType("datetime")
                .HasColumnName("datetime");
            entity.Property(e => e.Descr)
                .HasMaxLength(128)
                .HasColumnName("descr");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Lat).HasColumnName("lat");
            entity.Property(e => e.Lng).HasColumnName("lng");
            entity.Property(e => e.Pers)
                .HasMaxLength(45)
                .HasColumnName("pers");
            entity.Property(e => e.Status)
                .HasMaxLength(12)
                .HasColumnName("status");
            entity.Property(e => e.Url)
                .HasMaxLength(256)
                .HasColumnName("url");
        });

        modelBuilder.Entity<Leave>(entity =>
        {
            entity.HasKey(e => e.Idleave).HasName("PRIMARY");

            entity.ToTable("leave");

            entity.Property(e => e.Idleave)
                .HasColumnType("int(11)")
                .HasColumnName("idleave");
            entity.Property(e => e.Cover)
                .HasMaxLength(45)
                .HasColumnName("cover");
            entity.Property(e => e.Edate).HasColumnName("edate");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Pers)
                .HasMaxLength(45)
                .HasColumnName("pers");
            entity.Property(e => e.Sdate).HasColumnName("sdate");
            entity.Property(e => e.Shift)
                .HasMaxLength(12)
                .HasColumnName("shift");
            entity.Property(e => e.Status)
                .HasMaxLength(12)
                .HasColumnName("status");
            entity.Property(e => e.Type)
                .HasMaxLength(16)
                .HasColumnName("type");
            entity.Property(e => e.Updated).HasColumnName("updated");
        });

        modelBuilder.Entity<Menu>(entity =>
        {
            entity.HasKey(e => e.Idmenu).HasName("PRIMARY");

            entity.ToTable("menu");

            entity.HasIndex(e => e.Idmenu, "menu_id_UNIQUE").IsUnique();

            entity.Property(e => e.Idmenu)
                .HasColumnType("int(11) unsigned")
                .HasColumnName("idmenu");
            entity.Property(e => e.Access).HasColumnName("access");
            entity.Property(e => e.Active)
                .HasDefaultValueSql("'0'")
                .HasColumnType("int(11)")
                .HasColumnName("active");
            entity.Property(e => e.Cnt)
                .HasDefaultValueSql("'0'")
                .HasColumnType("int(11)")
                .HasColumnName("cnt");
            entity.Property(e => e.Content)
                .HasColumnType("text")
                .HasColumnName("content");
            entity.Property(e => e.Description)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("description");
            entity.Property(e => e.Image)
                .HasColumnType("mediumblob")
                .HasColumnName("image");
            entity.Property(e => e.Meta)
                .HasMaxLength(1024)
                .HasColumnName("meta");
            entity.Property(e => e.Parentid)
                .HasDefaultValueSql("'0'")
                .HasColumnType("int(11) unsigned")
                .HasColumnName("parentid");
            entity.Property(e => e.Role)
                .HasMaxLength(45)
                .HasColumnName("role");
            entity.Property(e => e.Screen)
                .HasMaxLength(2)
                .HasColumnName("screen");
            entity.Property(e => e.Show)
                .HasMaxLength(1)
                .IsFixedLength()
                .HasColumnName("show");
            entity.Property(e => e.Target)
                .HasMaxLength(45)
                .HasColumnName("target");
            entity.Property(e => e.Title)
                .HasMaxLength(256)
                .HasColumnName("title");
            entity.Property(e => e.Url)
                .HasMaxLength(200)
                .HasColumnName("url");
        });

        modelBuilder.Entity<Meter>(entity =>
        {
            entity.HasKey(e => e.Idmeter).HasName("PRIMARY");

            entity.ToTable("meter");

            entity.Property(e => e.Idmeter)
                .HasColumnType("int(11)")
                .HasColumnName("idmeter");
            entity.Property(e => e.Avg)
                .HasColumnType("int(11)")
                .HasColumnName("avg");
            entity.Property(e => e.High)
                .HasColumnType("smallint(6)")
                .HasColumnName("high");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Low)
                .HasColumnType("smallint(6)")
                .HasColumnName("low");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.Type)
                .HasMaxLength(4)
                .HasColumnName("type");
        });

        modelBuilder.Entity<Monitor>(entity =>
        {
            entity.HasKey(e => e.Idmonitor).HasName("PRIMARY");

            entity.ToTable("monitor");

            entity.Property(e => e.Idmonitor)
                .HasColumnType("int(11)")
                .HasColumnName("idmonitor");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Incident)
                .HasColumnType("tinyint(4)")
                .HasColumnName("incident");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.Type)
                .HasMaxLength(4)
                .HasColumnName("type");
        });

        modelBuilder.Entity<Org>(entity =>
        {
            entity.HasKey(e => e.Idorg).HasName("PRIMARY");

            entity.ToTable("org");

            entity.Property(e => e.Idorg)
                .HasColumnType("int(11)")
                .HasColumnName("idorg");
            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
        });

        modelBuilder.Entity<Patrol>(entity =>
        {
            entity.HasKey(e => e.Idpatrol).HasName("PRIMARY");

            entity.ToTable("patrol");

            entity.Property(e => e.Idpatrol)
                .HasColumnType("int(11)")
                .HasColumnName("idpatrol");
            entity.Property(e => e.Date).HasColumnName("date");
            entity.Property(e => e.Idpoint)
                .HasColumnType("int(11)")
                .HasColumnName("idpoint");
            entity.Property(e => e.Pers)
                .HasMaxLength(45)
                .HasColumnName("pers");
            entity.Property(e => e.Scan)
                .HasColumnType("time")
                .HasColumnName("scan");
            entity.Property(e => e.Time)
                .HasColumnType("time")
                .HasColumnName("time");
        });

        modelBuilder.Entity<Per>(entity =>
        {
            entity.HasKey(e => e.Idpers).HasName("PRIMARY");

            entity.ToTable("pers");

            entity.Property(e => e.Idpers)
                .HasColumnType("int(11)")
                .HasColumnName("idpers");
            entity.Property(e => e.Bank)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("bank");
            entity.Property(e => e.Bankacct)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("bankacct");
            entity.Property(e => e.Date).HasColumnName("date");
            entity.Property(e => e.Dob).HasColumnName("dob");
            entity.Property(e => e.Grade)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("grade");
            entity.Property(e => e.Idroute)
                .HasColumnType("int(11)")
                .HasColumnName("idroute");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.Nric)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("nric");
            entity.Property(e => e.Password)
                .HasMaxLength(45)
                .HasColumnName("password");
            entity.Property(e => e.Pers)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("pers");
            entity.Property(e => e.Shift)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("shift");
            entity.Property(e => e.Status)
                .IsRequired()
                .HasMaxLength(12)
                .HasColumnName("status");
            entity.Property(e => e.Telephone)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("telephone");
        });

        modelBuilder.Entity<Persd>(entity =>
        {
            entity.HasKey(e => e.Idpersd).HasName("PRIMARY");

            entity.ToTable("persd");

            entity.Property(e => e.Idpersd)
                .HasColumnType("int(11)")
                .HasColumnName("idpersd");
            entity.Property(e => e.Idpers)
                .HasColumnType("int(11)")
                .HasColumnName("idpers");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
        });

        modelBuilder.Entity<Photo>(entity =>
        {
            entity.HasKey(e => e.Idphoto).HasName("PRIMARY");

            entity.ToTable("photo");

            entity.Property(e => e.Idphoto)
                .HasColumnType("int(11)")
                .HasColumnName("idphoto");
            entity.Property(e => e.Channel)
                .HasColumnType("smallint(6)")
                .HasColumnName("channel");
            entity.Property(e => e.Datetime)
                .HasColumnType("datetime")
                .HasColumnName("datetime");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Photo1)
                .IsRequired()
                .HasMaxLength(256)
                .HasColumnName("photo");
            entity.Property(e => e.Preset)
                .HasColumnType("smallint(6)")
                .HasColumnName("preset");
            entity.Property(e => e.Status)
                .IsRequired()
                .HasMaxLength(12)
                .HasColumnName("status");
            entity.Property(e => e.Var)
                .HasPrecision(3, 2)
                .HasColumnName("var");
        });

        modelBuilder.Entity<Point>(entity =>
        {
            entity.HasKey(e => e.Idpoint).HasName("PRIMARY");

            entity.ToTable("point");

            entity.Property(e => e.Idpoint)
                .HasColumnType("int(11)")
                .HasColumnName("idpoint");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Lat).HasColumnName("lat");
            entity.Property(e => e.Lng).HasColumnName("lng");
            entity.Property(e => e.Name)
                .HasMaxLength(45)
                .HasColumnName("name");
        });

        modelBuilder.Entity<Reading>(entity =>
        {
            entity.HasKey(e => e.Idreading).HasName("PRIMARY");

            entity.ToTable("reading");

            entity.Property(e => e.Idreading)
                .HasColumnType("int(11)")
                .HasColumnName("idreading");
            entity.Property(e => e.Consump)
                .HasColumnType("int(11)")
                .HasColumnName("consump");
            entity.Property(e => e.Date).HasColumnName("date");
            entity.Property(e => e.Idmeter)
                .HasColumnType("int(11)")
                .HasColumnName("idmeter");
            entity.Property(e => e.Reading1)
                .HasColumnType("int(11)")
                .HasColumnName("reading");
            entity.Property(e => e.Status)
                .IsRequired()
                .HasMaxLength(6)
                .HasColumnName("status");
            entity.Property(e => e.Var)
                .HasPrecision(3, 2)
                .HasColumnName("var");
        });

        modelBuilder.Entity<Relief>(entity =>
        {
            entity.HasKey(e => e.Idrelief).HasName("PRIMARY");

            entity.ToTable("relief");

            entity.Property(e => e.Idrelief)
                .HasColumnType("int(11)")
                .HasColumnName("idrelief");
            entity.Property(e => e.Date)
                .HasMaxLength(45)
                .HasColumnName("date");
            entity.Property(e => e.Grade)
                .HasMaxLength(45)
                .HasColumnName("grade");
            entity.Property(e => e.Idpers)
                .HasColumnType("int(11)")
                .HasColumnName("idpers");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Pay)
                .HasPrecision(5)
                .HasColumnName("pay");
            entity.Property(e => e.Shift)
                .HasMaxLength(45)
                .HasColumnName("shift");
            entity.Property(e => e.Workhr)
                .HasPrecision(2)
                .HasColumnName("workhr");
        });

        modelBuilder.Entity<Route>(entity =>
        {
            entity.HasKey(e => e.Idroute).HasName("PRIMARY");

            entity.ToTable("route");

            entity.Property(e => e.Idroute)
                .HasColumnType("int(11)")
                .HasColumnName("idroute");
            entity.Property(e => e.Descr)
                .HasMaxLength(45)
                .HasColumnName("descr");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
        });

        modelBuilder.Entity<Routep>(entity =>
        {
            entity.HasKey(e => e.Idroutep).HasName("PRIMARY");

            entity.ToTable("routep");

            entity.Property(e => e.Idroutep)
                .HasColumnType("int(11)")
                .HasColumnName("idroutep");
            entity.Property(e => e.Idpoint)
                .HasColumnType("int(11)")
                .HasColumnName("idpoint");
            entity.Property(e => e.Idroute)
                .HasColumnType("int(11)")
                .HasColumnName("idroute");
            entity.Property(e => e.Time)
                .HasColumnType("time")
                .HasColumnName("time");
        });

        modelBuilder.Entity<Screenshot>(entity =>
        {
            entity.HasKey(e => e.Idscreenshot).HasName("PRIMARY");

            entity.ToTable("screenshot");

            entity.Property(e => e.Idscreenshot)
                .HasColumnType("int(11)")
                .HasColumnName("idscreenshot");
            entity.Property(e => e.Datetime)
                .HasColumnType("datetime")
                .HasColumnName("datetime");
            entity.Property(e => e.Location)
                .HasMaxLength(45)
                .HasColumnName("location");
            entity.Property(e => e.Screenshot1).HasColumnName("screenshot");
        });

        modelBuilder.Entity<Shift>(entity =>
        {
            entity.HasKey(e => e.Idshift).HasName("PRIMARY");

            entity.ToTable("shift");

            entity.Property(e => e.Idshift)
                .HasColumnType("int(11)")
                .HasColumnName("idshift");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Shift1)
                .HasMaxLength(45)
                .HasColumnName("shift");
        });

        modelBuilder.Entity<Site>(entity =>
        {
            entity.HasKey(e => e.Idsite).HasName("PRIMARY");

            entity.ToTable("site");

            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Idorg)
                .HasColumnType("int(11)")
                .HasColumnName("idorg");
            entity.Property(e => e.Meter)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("meter");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.Security)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("security");
            entity.Property(e => e.Site1)
                .IsRequired()
                .HasMaxLength(8)
                .HasColumnName("site");
        });

        modelBuilder.Entity<SiteRept>(entity =>
        {
            entity.HasKey(e => e.IdsiteRept).HasName("PRIMARY");

            entity.ToTable("site_rept");

            entity.Property(e => e.IdsiteRept)
                .HasColumnType("int(11)")
                .HasColumnName("idsite_rept");
            entity.Property(e => e.Date).HasColumnName("date");
            entity.Property(e => e.Idsite)
                .HasColumnType("int(11)")
                .HasColumnName("idsite");
            entity.Property(e => e.Url)
                .HasMaxLength(256)
                .HasColumnName("url");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.Iduser).HasName("PRIMARY");

            entity
                .ToTable("user")
                .HasCharSet("latin1")
                .UseCollation("latin1_swedish_ci");

            entity.Property(e => e.Iduser)
                .HasColumnType("int(11)")
                .HasColumnName("iduser");
            entity.Property(e => e.Access).HasColumnName("access");
            entity.Property(e => e.Cnt)
                .HasColumnType("int(11)")
                .HasColumnName("cnt");
            entity.Property(e => e.Code)
                .IsRequired()
                .HasMaxLength(16)
                .HasColumnName("code");
            entity.Property(e => e.Date).HasColumnName("date");
            entity.Property(e => e.Email)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("email");
            entity.Property(e => e.Idorg)
                .HasColumnType("int(11)")
                .HasColumnName("idorg");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasMaxLength(45)
                .HasColumnName("name");
            entity.Property(e => e.Password)
                .HasMaxLength(45)
                .HasColumnName("password");
            entity.Property(e => e.Role)
                .IsRequired()
                .HasMaxLength(6)
                .HasColumnName("role");
            entity.Property(e => e.Status)
                .IsRequired()
                .HasMaxLength(12)
                .HasColumnName("status");
            entity.Property(e => e.Telephone)
                .IsRequired()
                .HasMaxLength(12)
                .HasColumnName("telephone");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
