﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Relief
{
    public int Idrelief { get; set; }

    public string Date { get; set; }

    public int? Idsite { get; set; }

    public string Shift { get; set; }

    public string Grade { get; set; }

    public decimal? Pay { get; set; }

    public int Idpers { get; set; }

    public decimal Workhr { get; set; }
}
