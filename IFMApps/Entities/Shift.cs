﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Shift
{
    public int Idshift { get; set; }

    public int? Idsite { get; set; }

    public string Shift1 { get; set; }
}
