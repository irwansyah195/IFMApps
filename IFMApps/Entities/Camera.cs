﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Camera
{
    public int Idcamera { get; set; }

    public int? Idsite { get; set; }

    public short? Channel { get; set; }

    public short? Preset { get; set; }
}
