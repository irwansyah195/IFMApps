﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Meter
{
    public int Idmeter { get; set; }

    public int Idsite { get; set; }

    public string Name { get; set; }

    public string Type { get; set; }

    public int Avg { get; set; }

    public short? Low { get; set; }

    public short? High { get; set; }
}
