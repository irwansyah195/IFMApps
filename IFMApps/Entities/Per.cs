﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Per
{
    public int Idpers { get; set; }

    public string Pers { get; set; }

    public string Name { get; set; }

    public string Telephone { get; set; }

    public string Password { get; set; }

    public string Grade { get; set; }

    public int Idsite { get; set; }

    public string Shift { get; set; }

    public int Idroute { get; set; }

    public string Bankacct { get; set; }

    public string Bank { get; set; }

    public string Nric { get; set; }

    public DateOnly? Dob { get; set; }

    public string Status { get; set; }

    public DateOnly? Date { get; set; }
}
