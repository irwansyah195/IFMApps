﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class FieldSpec
{
    public int IdfieldSpec { get; set; }

    public int? IdtableSpec { get; set; }

    public string Name { get; set; }

    public string Label { get; set; }

    public short? Length { get; set; }

    public string Type { get; set; }

    public string Info { get; set; }

    public string Datasource { get; set; }

    public short? MaxChoice { get; set; }

    public ulong? Required { get; set; }
}
