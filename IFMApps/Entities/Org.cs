﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Org
{
    public int Idorg { get; set; }

    public string Name { get; set; }
}
