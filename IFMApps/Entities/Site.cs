﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Site
{
    public int Idsite { get; set; }

    public int Idorg { get; set; }

    public string Site1 { get; set; }

    public string Name { get; set; }

    public string Security { get; set; }

    public string Meter { get; set; }
}
