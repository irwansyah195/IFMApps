﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class FieldValue
{
    public int Id { get; set; }

    public string Field { get; set; }

    public string Value { get; set; }

    public string Descr { get; set; }
}
