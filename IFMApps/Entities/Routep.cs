﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Routep
{
    public int Idroutep { get; set; }

    public int? Idroute { get; set; }

    public int? Idpoint { get; set; }

    public TimeOnly? Time { get; set; }
}
