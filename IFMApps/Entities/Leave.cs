﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Leave
{
    public int Idleave { get; set; }

    public int? Idsite { get; set; }

    public string Pers { get; set; }

    public string Type { get; set; }

    public DateOnly? Sdate { get; set; }

    public DateOnly? Edate { get; set; }

    public string Shift { get; set; }

    public string Cover { get; set; }

    public string Status { get; set; }

    public DateOnly? Updated { get; set; }
}
