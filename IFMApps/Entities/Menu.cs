﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Menu
{
    public uint Idmenu { get; set; }

    public string Description { get; set; }

    public uint? Parentid { get; set; }

    public string Url { get; set; }

    public int? Active { get; set; }

    public string Role { get; set; }

    public string Target { get; set; }

    public string Content { get; set; }

    public byte[] Image { get; set; }

    public string Show { get; set; }

    public int? Cnt { get; set; }

    public DateOnly? Access { get; set; }

    public string Title { get; set; }

    public string Meta { get; set; }

    public string Screen { get; set; }
}
