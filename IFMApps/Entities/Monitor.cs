﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class Monitor
{
    public int Idmonitor { get; set; }

    public int Idsite { get; set; }

    public string Name { get; set; }

    public string Type { get; set; }

    public sbyte Incident { get; set; }
}
