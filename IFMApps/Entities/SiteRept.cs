﻿using System;
using System.Collections.Generic;

namespace IFMApps.Entities;

public partial class SiteRept
{
    public int IdsiteRept { get; set; }

    public DateOnly? Date { get; set; }

    public int? Idsite { get; set; }

    public string Url { get; set; }
}
